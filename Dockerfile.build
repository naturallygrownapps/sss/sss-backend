## This image prepares a build environment in one image, then creates another image that contains the build output.
## This build output image should be tagged as "sss-built". Running it runs the test suite.

## Build environment image
FROM mono:4.8 AS build-env
MAINTAINER Rufus Linke <mail@rufuslinke.de>
ARG BUILD_ID
LABEL build_id=$BUILD_ID

WORKDIR /sss

# Install mono build dependencies.
RUN apt-get update && apt-get install -y -qq nuget referenceassemblies-pcl

# Copy everything needed for nuget and perform restore as a distinct layer.
COPY SSS.Service.sln ./
COPY shared/sss-shared/SSS.Shared.csproj shared/sss-shared/
COPY sss-service/SSS.Service/SSS.Service.csproj sss-service/SSS.Service/packages.config sss-service/SSS.Service/
COPY sss-service/SSS.Service.Test/SSS.Service.Test.csproj sss-service/SSS.Service.Test/packages.config sss-service/SSS.Service.Test/
RUN nuget restore -NonInteractive

# Copy everything else and build.
COPY . ./
RUN xbuild /p:Configuration=Release SSS.Service.sln


## Image containing build output, it is prepared to be tested.
FROM mono:4.8
WORKDIR /sss
COPY --from=build-env /sss/sss-service/SSS.Service/bin/Release/ ./run/
COPY --from=build-env /sss/sss-service/SSS.Service.Test/bin/Release/ ./test/
COPY --from=build-env /sss/packages/ ./packages/
ENTRYPOINT ["mono", "./packages/NUnit.ConsoleRunner.3.6.1/tools/nunit3-console.exe", "./test/SSS.Service.Test.dll"]
