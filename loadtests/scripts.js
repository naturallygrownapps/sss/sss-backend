const crypto = require('crypto');

module.exports = {
    hashPassword: hashPassword
};

function hashPassword(requestParams, context, ee, next) {
    const pw = context.vars.password;
    context.vars.password = crypto.createHash('sha256').update(pw).digest('base64');
    next();
}
