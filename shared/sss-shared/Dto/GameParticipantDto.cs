﻿using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared
{
    public class GameParticipantDto
    {
        /// <summary>
        /// Represents a non-existing player.
        /// </summary>
        public static readonly GameParticipantDto Nobody = new GameParticipantDto()
        {
            Username = "nobody"
        };

        public string Username { get; set; }
        public byte[] PublicKey { get; set; }

        public GameParticipantDto()
        {
        }
    }
}