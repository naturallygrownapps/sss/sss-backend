﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared
{
    /// <summary>
    /// A game's status. Clients can use it to determine whether they won, lost or if a game is still active and 
    /// their action is required.
    /// </summary>
    public class GameStatus
    {
        public enum GameState
        {
            /// <summary>
            /// The game was started, but was not accepted yet.
            /// </summary>
            InitiatedByYou,
            /// <summary>
            /// You were challenged and are requested to accept the game.
            /// </summary>
            AcceptanceRequired,
            /// <summary>
            /// You are the challenger and are requested to place your bet (your opponent already provided it when accepting).
            /// </summary>
            ChallengerBetRequired,
            /// <summary>
            /// The client has to submit a move.
            /// </summary>
            YourMoveRequired,
            /// <summary>
            /// The other player has to submit a move.
            /// </summary>
            WaitingForOtherPlayer,
            /// <summary>
            /// The game was rejected by one of the players.
            /// </summary>
            Rejected,
            /// <summary>
            /// The game is over and the client won.
            /// </summary>
            YouWon,
            /// <summary>
            /// The game is over and the other player won.
            /// </summary>
            YouLost
        }

        public long Timestamp { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid GameId { get; set; }
        public GameState State { get; set; }
        public string[] RoundWinners { get; set; }
        public int RequiredWinningRounds { get; set; }
        public BetCapabilities? AgreedBetType { get; set; }
        public IEnumerable<BetCapabilities> BetTypeCaps { get; set; }
        public GameParticipantDto OtherPlayer { get; set; }
        public RoundDto[] Rounds { get; set; }
        public PlayerRole PlayerRole { get; set; }
        public int Revision { get; set; }
    }
}