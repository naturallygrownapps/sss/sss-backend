﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Shared.Parameters
{
    public class AuthModuleParameters
    {
        public class LoginInput
        {
            public string Username { get; set; }
            public string PwHash { get; set; }
        }

        public class LoginOutput
        {
            public string Token { get; set; }
            public string Username { get; set; }
            public byte[] Pk { get; set; }
        }

        public class RegisterInput
        {
            public string Username { get; set; }
            public string PwHash { get; set; }
            public byte[] Pk { get; set; } // salt for derivation + iv + encrypted private key
        }

        public class RegisterOutput
        {
            public Guid UserId { get; set; }
        }

        public class CheckTokenInput
        {
            public string Token { get; set; }
        }

        public class RegisterNotificationsInput
        {
            public string NotificationId { get; set; }
            public string UserPublicKey { get; set; }
            public NotificationPlatform Platform { get; set; }
        }

        public class DeleteNotificationsInput
        {
            public string NotificationId { get; set; }
        }
    }
}