﻿using MongoDB.Driver;
using NUnit.Framework;
using SSS.Service.Gaming;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace SSS.Service.Test
{
    [TestFixture]
    public class GameServiceTests : TestBase
    {
        private byte[] GenerateMockKey()
        {
            var key = new byte[2048];
            Random.NextBytes(key);
            return key;
        }

        protected override bool RequiresDatabase
        {
            get { return true; }
        }

        private void Init(Action<Autofac.ILifetimeScope> setupDependencies = null,
                          Action<IMongoCollection<Game>> gamsCollectionAction = null,
                          bool mockTokenization = true)
        {

            Action<Autofac.ILifetimeScope> localSetup = (container) =>
            {
                if (setupDependencies != null)
                    setupDependencies(container);
                if (gamsCollectionAction != null)
                    gamsCollectionAction(container.Resolve<Storage.IDatabase>().GetCollection<Game>("games"));
            };

            this.Init(localSetup, mockTokenization);
        }

        private Game[] RegisterGames(params Game[] games)
        {
            Init(container =>
            {
            }, gamesCollection =>
            {
                gamesCollection.InsertMany(games);
            });

            return games;
        }

        [Test]
        public void TestGetFindUserGames_Challenger_ExistingGameKey()
        {
            var firstGame = RegisterGames(
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                },
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                })
            .First();

            var gameSvc = Container.Resolve<IGameService>();
            var games = gameSvc.FindGamesForUser("challenger", firstGame.Challenger.PublicKey).ToList();

            Assert.AreEqual(1, games.Count);
            Assert.AreEqual(games[0].GameId, firstGame.GameId);
        }


        [Test]
        public void TestGetFindUserGames_Challenger_InvalidGameKey()
        {
            RegisterGames(
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                },
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                });

            var gameSvc = Container.Resolve<IGameService>();
            var games = gameSvc.FindGamesForUser("challenger", GenerateMockKey()).ToList();

            Assert.AreEqual(0, games.Count);
        }
        
        [Test]
        public void TestGetFindUserGames_Opponent_ExistingGameKey()
        {
            var secondGame = RegisterGames(
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                },
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                })
            .Skip(1).First();

            var gameSvc = Container.Resolve<IGameService>();
            var games = gameSvc.FindGamesForUser("opponent", secondGame.Opponent.PublicKey).ToList();

            Assert.AreEqual(1, games.Count);
            Assert.AreEqual(games[0].GameId, secondGame.GameId);
        }


        [Test]
        public void TestGetFindUserGames_Opponent_InvalidGameKey()
        {
            RegisterGames(
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                },
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "opponent"
                    }
                });

            var gameSvc = Container.Resolve<IGameService>();
            var games = gameSvc.FindGamesForUser("opponent", GenerateMockKey()).ToList();

            Assert.AreEqual(0, games.Count);
        }


        [Test]
        public void TestGetFindUserGames_Opponent_NonAcceptedGame()
        {
            var firstGame = RegisterGames(
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GenerateMockKey(),
                        Username = "challenger"
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = null,
                        Username = "opponent"
                    }
                })
            .First();

            var gameSvc = Container.Resolve<IGameService>();
            var games = gameSvc.FindGamesForUser("opponent", GenerateMockKey()).ToList();

            Assert.AreEqual(1, games.Count);
            Assert.AreEqual(games[0].GameId, firstGame.GameId);
        }
    }
}
