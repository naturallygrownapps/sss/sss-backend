﻿using System;
using NUnit.Framework;
using SSS.Service.Auth;
using Moq;
using SSS.Service.Gaming;
using Nancy;
using Nancy.Testing;
using System.Collections.Generic;
using SSS.Shared;
using SSS.Shared.Parameters;
using MongoDB.Driver;
using System.Linq;
using SSS.Service.Notifications;

namespace SSS.Service.Test
{
    [TestFixture]
    public class GameTests : TestBase
    {
        private Mock<IGameService> _gameSvcMock;
        private Mock<IGameStatusNotifier> _gameStatusNotifierMock;

        protected override void Init(Action<Autofac.ILifetimeScope> setupDependencies = null,
                                     bool mockTokenization = true)
        {
            Action<Autofac.ILifetimeScope> localSetup = (container) =>
            {
                var userSvcMock = new Mock<IUserService>();
                userSvcMock.Setup(s => s.FindUser("challenger")).Returns(new UserEntity()
                {
                    Username = "challenger"
                }); 
                userSvcMock.Setup(s => s.FindUser("opponent")).Returns(new UserEntity()
                {
                    Username = "opponent"
                });
                OverrideDependency(container, userSvcMock.Object);

                _gameSvcMock = new Mock<IGameService>();
                OverrideDependency(container, _gameSvcMock.Object);

                _gameStatusNotifierMock = new Mock<IGameStatusNotifier>(MockBehavior.Loose);
                OverrideDependency(container, _gameStatusNotifierMock.Object);

                if (setupDependencies != null)
                    setupDependencies(container);
            };

            base.Init(localSetup, mockTokenization);
        }

        [Test]
        public void TestChallenge()
        {
            Game addedGame = null;
            Init(container =>
                {
                    _gameSvcMock.Setup(s => s.AddGame(It.IsAny<Game>())).
                                 Callback<Game>(g => addedGame = g);

                    AuthenticateAs("challenger");
                });

            var response = Browser.Post("/game/challenge", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    challengedUsername = "opponent",
                    publicKey = new byte[1],
                    betTypeCaps = new[] { BetCapabilities.TextMessage }
                });
                WithPreconfiguredAuthentication(with);
            });
            var output = response.Body.DeserializeJson<GameStatus>();

            _gameStatusNotifierMock.Verify(n => n.NotifyNewGameStarted(It.IsAny<Game>(), It.Is<GameParticipant>(p => p.Username == "opponent")));

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.AreEqual(output.State, GameStatus.GameState.InitiatedByYou);
            Assert.AreEqual(output.PlayerRole, PlayerRole.Challenger);
        }

        [Test]
        public void TestChallengeSelf()
        {
            Init(container =>
            {
                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/challenge", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    challengedUsername = "challenger",
                    publicKey = new byte[1],
                    betTypeCaps = new[] { BetCapabilities.TextMessage }
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void TestChallengeUnknownUser()
        {
            Init(container =>
            {
                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/challenge", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    challengedUsername = "i_dont_exist",
                    move = "rock",
                    publicKey = new byte[1]
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Test]
        public void TestChallengeWithoutCapabilities()
        {
            Init(container =>
            {
                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/challenge", ctx =>
            {
                WithDefaults(ctx);
                WithJsonBody(ctx, new
                {
                    challengedUsername = "opponent",
                    move = "rock",
                    publicKey = new byte[1]
                });
                WithPreconfiguredAuthentication(ctx);
            });

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void TestChallengeExistingGame()
        {
            var key = GetRandomBytes(1);
            Game existingGame = Game.StartNew("challenger", key, new[] { BetCapabilities.Photo }, "opponent");
            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGamesForUser("challenger", key))
                            .Returns(new[] { existingGame });
                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/challenge", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    challengedUsername = "opponent",
                    publicKey = key,
                    betTypeCaps = new[] { BetCapabilities.TextMessage }
                });
                WithPreconfiguredAuthentication(with);
            });
            response.ShouldHaveRedirectedTo("/game/" + Service.Utils.GuidTools.ToString(existingGame.GameId));

            Assert.AreEqual(HttpStatusCode.SeeOther, response.StatusCode);

            var returnedStatus = response.Body.DeserializeJson<GameStatus>();
            var expectedStatus = existingGame.GetStatus("challenger");
            Assert.AreEqual(expectedStatus.GameId, returnedStatus.GameId);

            // Ignore timestamp.
            expectedStatus.Timestamp = 0;
            returnedStatus.Timestamp = 0;
            Assert.AreEqual(Serialize(expectedStatus), Serialize(returnedStatus));
        }

        [Test]
        public void TestOpponentAcceptStatus()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            var status = game.GetStatus("opponent");
            Assert.AreEqual(GameStatus.GameState.AcceptanceRequired, status.State);
        }

        [Test]
        public void TestAccept()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("opponent");
            });
 
            var myBet = GetRandomBet();
            var oldRevision = game.Revision;
            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentaccept", with =>
                {
                    WithDefaults(with);
                    WithJsonBody(with, new
                        {
                            gameId = game.GameId,
                            publicKey = GetRandomBytes(100),
                            betType = BetCapabilities.TextMessage,
                            bet = myBet
                        });
                    WithPreconfiguredAuthentication(with);
                });

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(game.Opponent.Username, "opponent");
            CollectionAssert.AreEqual(myBet.Data, game.Opponent.Bet.Data);
            CollectionAssert.AreEqual(myBet.ReceiverKeyAndIV, game.Opponent.Bet.ReceiverKeyAndIV);
            CollectionAssert.AreEqual(myBet.SourceKeyAndIV, game.Opponent.Bet.SourceKeyAndIV);
            Assert.IsNotNull(game.Opponent.PublicKey);

            Assert.AreEqual(0, game.CurrentRound, "The first round should have been started.");
            Assert.AreEqual(1, game.Rounds.Count, "The first round should have been started.");

            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YourMoveRequired, body.State);
            Assert.AreEqual(body.PlayerRole, PlayerRole.Opponent);
            Assert.IsNull(body.Rounds[0].YourMove);
            Assert.IsNull(body.Rounds[0].OtherPlayerMove);
            
            Assert.AreEqual(oldRevision + 1, body.Revision);

            _gameStatusNotifierMock.Verify(n => n.NotifyGameStatusChanged(It.IsAny<Game>(), It.Is<GameParticipant>(p => p.Username == "challenger")));

            // Check if the game is in the correct state from the challenger perspective.
            AuthenticateAs("challenger");
            response = Browser.Get("/game/" + game.GameId.ToString(), with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.ChallengerBetRequired, body.State);
        }

        [Test]
        public void TestAcceptWithInvalidBetTypeProposal()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("opponent");
            });

            var myPublicKey = new byte[20];
            new Random().NextBytes(myPublicKey);
            var myBet = GetRandomBet();
            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentaccept", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    gameId = game.GameId,
                    publicKey = myPublicKey,
                    betType = BetCapabilities.Photo,
                    bet = myBet
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Test]
        public void TestAcceptForeignGame()
        {
            var game = Game.StartNew("challenger", null, new[] { BetCapabilities.TextMessage }, "opponent");
            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);

                AuthenticateAs("foreigner");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentaccept", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    gameId = game.GameId
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Test]
        public void TestReject()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("opponent");
            });

            var oldRevision = game.Revision;
            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentreject", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(game.Opponent.Username, "opponent");
            Assert.IsTrue(game.IsRejected, "The game should have been rejected.");
            Assert.IsTrue(game.Opponent.HasCompletedGame, "The game should have been completed for the opponent.");
            Assert.IsNull(game.Opponent.PublicKey);
            Assert.AreEqual(-1, game.CurrentRound, "The game should not have started a round.");

            _gameStatusNotifierMock.Verify(n => n.NotifyGameRejected(It.IsAny<Game>(), It.Is<GameParticipant>(p => p.Username == "challenger")));

            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.Rejected, body.State);
            Assert.AreEqual(oldRevision + 1, body.Revision);
        }

        [Test]
        public void TestRejectForeignGame()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("foreigner");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentreject", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            Assert.IsFalse(game.IsRejected);
            Assert.AreEqual(GameStatus.GameState.InitiatedByYou, game.GetStatus("challenger").State);
        }

        [Test]
        public void TestRejectStartedGame()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            // Simulate accepting the game.
            game.Accept(new byte[1], GetRandomBet(), BetCapabilities.TextMessage);

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("opponent");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/opponentreject", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            Assert.IsFalse(game.IsRejected);
            Assert.AreEqual(GameStatus.GameState.YourMoveRequired, game.GetStatus("opponent").State);
        }

        [Test]
        public void TestAcceptRejection()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            // Simulate rejecting the game.
            game.RejectByOpponent();

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.RemoveGame(game));

                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/acceptrejection", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.NoContent, response.StatusCode);
            _gameSvcMock.Verify(s => s.RemoveGame(game), Times.Once());
        }

        [Test]
        public void TestAcceptRejectionOnUnrejectedGame()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.RemoveGame(game));

                AuthenticateAs("challenger");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/acceptrejection", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
            _gameSvcMock.Verify(s => s.RemoveGame(game), Times.Never());
        }

        [Test]
        public void TestAcceptRejectionAsOpponent()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            // Simulate rejecting the game.
            game.RejectByOpponent();

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.RemoveGame(game));

                AuthenticateAs("opponent");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/acceptrejection", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            _gameSvcMock.Verify(s => s.RemoveGame(game), Times.Never());
        }

        [Test]
        public void TestAcceptRejectionAsForeigner()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            // Simulate rejecting the game.
            game.RejectByOpponent();

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.RemoveGame(game));

                AuthenticateAs("foreigner");
            });

            var response = Browser.Post("/game/" + game.GameId.ToString() + "/acceptrejection", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            _gameSvcMock.Verify(s => s.RemoveGame(game), Times.Never());
        }

        [Test]
        public void TestChallengerBet()
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            game.Accept(GetRandomBytes(100), GetRandomBet(), BetCapabilities.TextMessage);
            Assert.IsNull(game.Challenger.Bet);

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("challenger");
            });

            var myBet = GetRandomBet();
            var oldRevision = game.Revision;
            var response = Browser.Post("/game/" + game.GameId.ToString() + "/challengerbet", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    gameId = game.GameId,
                    bet = myBet
                });
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            CollectionAssert.AreEqual(myBet.Data, game.Challenger.Bet.Data);
            CollectionAssert.AreEqual(myBet.ReceiverKeyAndIV, game.Challenger.Bet.ReceiverKeyAndIV);
            CollectionAssert.AreEqual(myBet.SourceKeyAndIV, game.Challenger.Bet.SourceKeyAndIV);
            Assert.AreEqual(oldRevision + 1, game.Revision);
        }

        [Test]
        public void TestGetBet()
        {
            var challengerBet = GetRandomBet();
            // Create a game that the opponent has won.
            Game game = new Game()
            {
                GameId = Guid.NewGuid(),
                Challenger = new GameParticipant()
                {
                    PublicKey = GetRandomBytes(100),
                    Username = "challenger",
                    Bet = challengerBet
                },
                Opponent = new GameParticipant()
                {
                    PublicKey = GetRandomBytes(100),
                    Username = "opponent",
                    Bet = GetRandomBet()
                },
                CurrentRound = 2,
                Rounds = new List<Round> 
                { 
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper }
                }
            };
            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("opponent");
            });

            var response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var body = DeserializeBody<EncryptedBet>(response);
            // Assert that the correct bet (the challenger's because he lost) was sent.
            CollectionAssert.AreEqual(challengerBet.Data, body.Data);
            CollectionAssert.AreEqual(challengerBet.ReceiverKeyAndIV, body.ReceiverKeyAndIV);
            CollectionAssert.AreEqual(challengerBet.SourceKeyAndIV, body.SourceKeyAndIV);

            AuthenticateAs("challenger");

            response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            body = DeserializeBody<EncryptedBet>(response);
            // Assert that the correct bet (the challenger's because he lost) was sent.
            CollectionAssert.AreEqual(challengerBet.Data, body.Data);
            CollectionAssert.AreEqual(challengerBet.ReceiverKeyAndIV, body.ReceiverKeyAndIV);
            CollectionAssert.AreEqual(challengerBet.SourceKeyAndIV, body.SourceKeyAndIV);
        }

        [Test]
        public void TestGetBetForeignUser()
        {
            Game game = new Game()
            {
                GameId = Guid.NewGuid(),
                Challenger = new GameParticipant()
                {
                    PublicKey = new byte[1],
                    Username = "challenger"
                },
                Opponent = new GameParticipant()
                {
                    PublicKey = new byte[1],
                    Username = "opponent"
                },
                CurrentRound = 2,
                Rounds = new List<Round> 
                { 
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper }
                }
            };
            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));

                AuthenticateAs("foreigner");
            });

            var response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            var body = DeserializeBody<EncryptedBet>(response);
            Assert.IsNull(body.Data);
            Assert.IsNull(body.ReceiverKeyAndIV);
            Assert.IsNull(body.SourceKeyAndIV);
        }

        /// <summary>
        /// Initializes for testing with a game with the players "challenger" and "opponent".
        /// Challenger has started the game. If <paramref name="isAccepted"/> is true,
        /// challenger made move "rock" and opponent has accepted with "paper" (i.e. opponent has won the first round).
        /// </summary>
        private Game InitGame(bool isAccepted = true)
        {
            var game = Game.StartNew("challenger", new byte[1], new[] { BetCapabilities.TextMessage }, "opponent");
            if (isAccepted)
            {
                game.Accept(GetRandomBytes(100), GetRandomBet(), BetCapabilities.TextMessage);
                game.SetChallengerBet(GetRandomBet());
                game.MakeMove("challenger", Move.Rock);
                game.MakeMove("opponent", Move.Paper);
            }

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));
            });

            return game;
        }

        private BrowserResponse PostMove(Game game, Move move)
        {
            var response = Browser.Post("/game/" + game.GameId.ToString() + "/move", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new
                {
                    gameId = game.GameId,
                    move = Enum.GetName(typeof(Move), move).ToLower()
                });
                WithPreconfiguredAuthentication(with);
            });
            return response;
        }

        [Test]
        public void TestMove()
        {
            var game = InitGame();
            AuthenticateAs("opponent");

            var oldRevision = game.Revision;

            var response = PostMove(game, Move.Paper);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.WaitingForOtherPlayer, body.State);
            Assert.AreEqual(Move.Paper, body.Rounds[body.Rounds.Length - 1].YourMove);
            Assert.IsNull(body.Rounds[body.Rounds.Length - 1].OtherPlayerMove);
            
            Assert.AreEqual(oldRevision + 1, body.Revision);

            _gameStatusNotifierMock.Verify(n => n.NotifyGameStatusChanged(It.IsAny<Game>(), It.Is<GameParticipant>(p => p.Username == "challenger")));
        }

        [Test]
        public void TestDrawMoves()
        {
            var game = InitGame();
            var oldRevision = game.Revision;

            AuthenticateAs("opponent");
            PostMove(game, Move.Paper);

            AuthenticateAs("challenger");
            var response = PostMove(game, Move.Paper);
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, GetErrorMessage(response));
            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YourMoveRequired, body.State);

            // We should be in the third round now (first is through acceptance, second is draw, third just started).
            Assert.AreEqual(2, game.CurrentRound);
            Assert.AreEqual(3, game.Rounds.Count);
            Assert.IsFalse(game.Rounds[2].IsFinished);
            // The previous round should be finished.
            Assert.IsTrue(game.Rounds[1].IsFinished);
            // The round must not have a winner.
            Assert.AreEqual(GameParticipant.Nobody, game.Rounds[1].GetWinner(game.Challenger, game.Opponent));

            // Two moves made.
            Assert.AreEqual(oldRevision + 2, body.Revision);
        }

        [Test]
        public void TestMoveNonAcceptedGame()
        {
            var game = InitGame(false);
            AuthenticateAs("opponent");

            var response = PostMove(game, Move.Paper);

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Test]
        public void TestMoveWithoutPlacedChallengerBet()
        {
            var game = InitGame(false);
            game.Accept(GetRandomBytes(100), GetRandomBet(), BetCapabilities.TextMessage);

            AuthenticateAs("challenger");
            var response = PostMove(game, Move.Paper);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, GetErrorMessage(response));

            Assert.AreEqual(0, game.CurrentRound);
            var status = game.GetStatus("challenger");
            Assert.AreEqual(GameStatus.GameState.ChallengerBetRequired, status.State);
        }

        [Test]
        public void TestMoveForeigner()
        {
            var game = InitGame();
            AuthenticateAs("foreigner");

            var response = PostMove(game, Move.Paper);

            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Test]
        public void TestMoveOnFinishedGame()
        {
            var game = InitGame();

            // Finish the game.
            for (int i = 0; i < Game.RequiredWinningRounds - 1; i++)
            {
                AuthenticateAs("opponent");
                PostMove(game, Move.Paper);
                AuthenticateAs("challenger");
                PostMove(game, Move.Rock);
            }

            // Try another move, it should fail for both players.
            AuthenticateAs("challenger");
            var response = PostMove(game, Move.Paper);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);

            AuthenticateAs("opponent");
            response = PostMove(game, Move.Paper);
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
        }

        [Test]
        public void TestGameFinishAfterMinRoundWins()
        {
            // The initialization already performs round 1 through acceptance.
            // Opponent won the first round.
            var game = InitGame();

            // Let opponent win as many times as necessary.
            BrowserResponse response = null;
            for (int i = 0; i < Game.RequiredWinningRounds - 1; i++)
            {
                AuthenticateAs("opponent");
                PostMove(game, Move.Paper);
                AuthenticateAs("challenger");
                response = PostMove(game, Move.Rock);
            }

            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(game.IsFinished);
            Assert.AreEqual(Game.RequiredWinningRounds, game.Rounds.Count);
            // CurrentRound is index-based.
            Assert.AreEqual(Game.RequiredWinningRounds, game.CurrentRound + 1);

            // We still look from the challenger's perspective. He should have lost.
            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YouLost, body.State);

            // Check if the opponent gets the win flag.
            AuthenticateAs("opponent");
            response = Browser.Get("/game/" + game.GameId.ToString(), with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YouWon, body.State);
        }

        [Test]
        public void TestGameFinishWithDraws()
        {
            // The initialization already performs round 1 through acceptance.
            // Opponent won the first round.
            var game = InitGame();

            // Simulate a draw round.
            AuthenticateAs("opponent");
            PostMove(game, Move.Paper);
            AuthenticateAs("challenger");
            PostMove(game, Move.Paper);

            // Let opponent win as many times as necessary.
            BrowserResponse response = null;
            for (int i = 0; i < Game.RequiredWinningRounds - 1; i++)
            {
                AuthenticateAs("opponent");
                PostMove(game, Move.Paper);
                AuthenticateAs("challenger");
                response = PostMove(game, Move.Rock);
            }

            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.IsTrue(game.IsFinished);
            // We should have one more round than necessary now (the draw round).
            Assert.AreEqual(Game.RequiredWinningRounds + 1, game.Rounds.Count);
            Assert.AreEqual(Game.RequiredWinningRounds + 1, game.CurrentRound + 1);

            // We still look from the challenger's perspective. He should have lost.
            var body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YouLost, body.State);

            // Check if the opponent gets the win flag.
            AuthenticateAs("opponent");
            response = Browser.Get("/game/" + game.GameId.ToString(), with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            body = DeserializeBody<GameStatus>(response);
            Assert.AreEqual(GameStatus.GameState.YouWon, body.State);
        }

        [Test]
        public void TestGameSetToCompleted()
        {
            var challengerBet = GetRandomBet();
            // Create a game that the opponent has won.
            Game game = new Game()
            {
                GameId = Guid.NewGuid(),
                Challenger = new GameParticipant()
                {
                    PublicKey = GetRandomBytes(100),
                    Username = "challenger",
                    Bet = challengerBet
                },
                Opponent = new GameParticipant()
                {
                    PublicKey = GetRandomBytes(100),
                    Username = "opponent",
                    Bet = GetRandomBet()
                },
                CurrentRound = 2,
                Rounds = new List<Round>
                {
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                    new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper }
                }
            };

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(game.GameId)).
                             Returns(game);
                _gameSvcMock.Setup(s => s.UpdateGame(game));
                _gameSvcMock.Setup(s => s.RemoveGame(game)).Verifiable();

                AuthenticateAs("opponent");
            });

            var response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            // The HasCompletedGame flag should only be set when the body has been sent.
            Assert.IsFalse(game.Opponent.HasCompletedGame);
            Assert.IsFalse(game.Challenger.HasCompletedGame);
            DeserializeBody<EncryptedBet>(response);
            Assert.IsTrue(game.Opponent.HasCompletedGame);
            Assert.IsFalse(game.Challenger.HasCompletedGame);

            // Try to request the bet again, it should fail.
            response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            Assert.AreEqual(HttpStatusCode.Forbidden, response.StatusCode);
            DeserializeBody<EncryptedBet>(response);

            // Game should not have been changed.
            Assert.IsTrue(game.Opponent.HasCompletedGame);
            Assert.IsFalse(game.Challenger.HasCompletedGame);

            // But the other player should still be able to obtain his bet.
            AuthenticateAs("challenger");
            response = Browser.Get("/game/" + game.GameId.ToString() + "/bet", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
            });
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            DeserializeBody<EncryptedBet>(response);

            Assert.IsTrue(game.Challenger.HasCompletedGame);
            Assert.IsTrue(game.Opponent.HasCompletedGame);

            // The game should have been removed as both players finished.
            _gameSvcMock.Verify(s => s.RemoveGame(game), Times.Exactly(1));
        }
        
        private IEnumerable<GameStatus> TestDoNotFindCompletedGames(string authenticateAs, bool challengerCompleted, bool opponentCompleted)
        {
            // Create two games. For the first game, we have obtained the bet after it was finished.
            var games = new[] {
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GetRandomBytes(100),
                        Username = "challenger",
                        Bet = GetRandomBet(),
                        HasCompletedGame = challengerCompleted
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GetRandomBytes(100),
                        Username = "opponent",
                        Bet = GetRandomBet(),
                        HasCompletedGame = opponentCompleted
                    },
                    CurrentRound = 2,
                    Rounds = new List<Round>
                    {
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper }
                    }
                },
                new Game()
                {
                    GameId = Guid.NewGuid(),
                    Challenger = new GameParticipant()
                    {
                        PublicKey = GetRandomBytes(100),
                        Username = "challenger",
                        Bet = GetRandomBet(),
                        HasCompletedGame = false
                    },
                    Opponent = new GameParticipant()
                    {
                        PublicKey = GetRandomBytes(100),
                        Username = "opponent",
                        Bet = GetRandomBet(),
                        HasCompletedGame = false
                    },
                    CurrentRound = 2,
                    Rounds = new List<Round>
                    {
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper },
                        new Round() { ChallengerMove = Move.Rock, OpponentMove = Move.Paper }
                    }
                }
            };

            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGamesForUser("challenger", It.IsAny<byte[]>())).Returns(games);
                AuthenticateAs(authenticateAs);
            });

            var response = Browser.Post("/game/mygames", with =>
            {
                WithDefaults(with);
                WithPreconfiguredAuthentication(with);
                WithJsonBody(with, new
                {
                    gameKey = GetRandomBytes(32)
                });
            });
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            return DeserializeBody<IEnumerable<GameStatus>>(response);
        }

        [Test]
        public void TestDoNotFindCompletedGames_NoneCompleted()
        {
            var receivedGames = TestDoNotFindCompletedGames("challenger", false, false);
            Assert.AreEqual(2, receivedGames.Count());
        }

        [Test]
        public void TestDoNotFindCompletedGames_SelfCompleted()
        {
            var receivedGames = TestDoNotFindCompletedGames("challenger", true, false);
            Assert.AreEqual(1, receivedGames.Count());
        }

        [Test]
        public void TestDoNotFindCompletedGames_OtherCompleted()
        {
            var receivedGames = TestDoNotFindCompletedGames("challenger", false, true);
            Assert.AreEqual(2, receivedGames.Count());
        }

        [Test]
        public void TestDoNotFindCompletedGames_BothCompleted()
        {
            var receivedGames = TestDoNotFindCompletedGames("challenger", true, true);
            Assert.AreEqual(1, receivedGames.Count());
        }

        [Test]
        public void TestTimestamps()
        {
            var game = Game.StartNew("challenger", GetRandomBytes(1), new[] { BetCapabilities.Photo }, "opponent");
            Init(container =>
            {
                _gameSvcMock.Setup(s => s.FindGame(It.Is<Guid>(g => g.Equals(game.GameId)))).Returns(game);
                AuthenticateAs("challenger");
            });

            var response = Browser.Get("/game/" + game.GameId.ToString("N"), ctx =>
            {
                WithDefaults(ctx);
                WithPreconfiguredAuthentication(ctx);
            });
            var timestamp1 = response.Body.DeserializeJson<GameStatus>().Timestamp;

            response = Browser.Get("/game/" + game.GameId.ToString("N"), ctx =>
            {
                WithDefaults(ctx);
                WithPreconfiguredAuthentication(ctx);
            });
            var timestamp2 = response.Body.DeserializeJson<GameStatus>().Timestamp;

            Assert.Greater(timestamp2, timestamp1, "The second query for a GameStatus should have a newer timestamp.");
        }
    }
}
