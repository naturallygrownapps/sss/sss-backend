﻿using Autofac;
using System;
using NUnit.Framework;
using Newtonsoft.Json;
using System.IO;

namespace SSS.Service.Test
{
    [TestFixture]
    public class JsonTests : TestBase
    {
        private class ClassWithProperty
        {
            public string Name { get; set; }
        }

        [SetUp]
        public void TestInitialize()
        {
            Init(null);
        }

        [Test]
        public void TestLowerCaseDeserialization()
        {
            var serializer = Container.Resolve<JsonSerializer>();
            var input = "{ \"name\": \"abc\" }";

            var reader = new JsonTextReader(new StringReader(input));
            var deserialized = serializer.Deserialize<ClassWithProperty>(reader);

            Assert.AreEqual("abc", deserialized.Name);
        }
    }
}
