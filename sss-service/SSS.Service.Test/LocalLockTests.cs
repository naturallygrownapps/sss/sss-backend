﻿using Autofac;
using log4net;
using Moq;
using NUnit.Framework;
using SSS.Service.Utils.Locking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SSS.Service.Test
{
    [TestFixture]
    public class LocalLockTests: TestBase
    {
        [SetUp]
        public void TestInitialize()
        {
            Init(null);
        }

        [Test]
        public async Task TestLocking()
        {
            var lockFactory = new LocalLockFactory(It.IsAny<ILog>());
            var resource = "TESTRESOURCE";

            DateTime t1ExecTime = new DateTime();
            var t1 = Task.Run(async () =>
            {
                using (var l = await lockFactory.LockAsyncWithDefaults(resource))
                {
                    Assert.IsTrue(l.IsAcquired);
                    t1ExecTime = DateTime.Now;
                    await Task.Delay(2000);
                }
            });

            await Task.Delay(1000);

            DateTime t2ExecTime = new DateTime();
            var t2 = Task.Run(async () =>
            {
                using (var l = await lockFactory.LockAsyncWithDefaults(resource))
                {
                    Assert.IsTrue(l.IsAcquired);
                    t2ExecTime = DateTime.Now;
                }
            });
            await t2;

            var diff = t2ExecTime - t1ExecTime;
            Assert.Greater(diff.TotalSeconds, 2.0);
        }

        [Test]
        public async Task TestConcurrentLocks()
        {
            var lockFactory = new LocalLockFactory(It.IsAny<ILog>());
            var resource = "TESTRESOURCE";

            const int NumTasks = 10;
            var activeTasks = 0;
            var tasksFinished = 0;
            
            await Task.WhenAll(Enumerable.Range(1, NumTasks).Select(_ =>
                Task.Run(async () =>
                {
                    using (var l = await lockFactory.LockAsync(resource, TimeSpan.FromHours(1), TimeSpan.FromHours(1), TimeSpan.FromMilliseconds(500)))
                    {
                        Assert.IsTrue(l.IsAcquired);
                        Interlocked.Increment(ref activeTasks);
                        Assert.AreEqual(1, activeTasks);
                        await Task.Delay(200);
                        Interlocked.Decrement(ref activeTasks);
                        Interlocked.Increment(ref tasksFinished);
                    }
                })
            ));

            Assert.AreEqual(NumTasks, tasksFinished);
        }

        [Test]
        public async Task TestLockWaitTimeout()
        {
            var lockFactory = new LocalLockFactory(It.IsAny<ILog>());
            var resource = "TESTRESOURCE";

            var t1 = Task.Run(async () =>
            {
                using (var l = await lockFactory.LockAsyncWithDefaults(resource))
                {
                    Assert.IsTrue(l.IsAcquired);
                    await Task.Delay(3000);
                }
            });
            
            await Task.Delay(1000);

            await Task.Run(async () =>
            {
                using (var l = await lockFactory.LockAsync(resource, TimeSpan.FromHours(1), TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(500)))
                {
                    Assert.IsFalse(l.IsAcquired);
                }
            });
        }

        [Test]
        public async Task TestLockExpiry()
        {
            var lockFactory = new LocalLockFactory(It.IsAny<ILog>(), TimeSpan.FromMilliseconds(500));
            var resource = "TESTRESOURCE";

            var t1 = Task.Run(async () =>
            {
                using (var l = await lockFactory.LockAsync(resource, TimeSpan.FromMilliseconds(100), TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1)))
                {
                    Assert.IsTrue(l.IsAcquired);
                    await Task.Delay(5000);
                }
            });

            await Task.Delay(2000);

            var isStillLocked = lockFactory.IsLocked(resource);
            Assert.IsFalse(isStillLocked);
        }
    }
}
