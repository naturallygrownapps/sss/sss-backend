﻿using NUnit.Framework;
using Moq;
using MongoDB.Bson;
using SSS.Service.Auth;
using Nancy.Testing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Security;
using SSS.Shared.Parameters;
using Autofac;

namespace SSS.Service.Test
{
    [TestFixture]
    public class LoginTests : TestBase
    {
        public class RequireAuthModule : NancyModule
        {
            public RequireAuthModule()
            {
                Get["/authrequired"] = _ => { this.RequiresAuthentication(); return this.Context.CurrentUser.UserName; };
            }
        }

        [Test]
        public void TestLogin()
        {
            var clientSideHashedPassword = "sdfsfdhsjk";
            // Setup
            Init(container =>
                {
                    var userMock = new Mock<IUserService>();
                    userMock.Setup(s => s.FindUser(It.IsAny<string>())).Returns(new UserEntity()
                        {
                            Username = "rufus",
                            PwHash = (new Scrypt.ScryptEncoder()).Encode(clientSideHashedPassword)
                        });
                    OverrideDependency<IUserService>(container, userMock.Object);
                }, mockTokenization: false);

            // Login
            var response = Browser.Post("/auth/login", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new 
                {
                    username = "rufus",
                    pwhash = clientSideHashedPassword
                });
            });
            var loginResponse = response.Body.DeserializeJson<AuthModuleParameters.LoginOutput>();

            // Should have received a token.
            Assert.AreEqual(Nancy.HttpStatusCode.OK, response.StatusCode);
            Assert.IsFalse(string.IsNullOrWhiteSpace(loginResponse.Token));

            // Send token to check if authentication was successful.
            response = Browser.Get("/authrequired", with =>
            {
                WithDefaults(with);
                with.Header("Authorization", "Token " + loginResponse.Token);
            });

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual("rufus", response.Body.AsString());
        }
    }
}
