﻿using Moq;
using NUnit.Framework;
using SSS.Service.Auth;
using SSS.Service.Gaming;
using SSS.Service.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Newtonsoft.Json.Linq;
using SSS.Shared;

namespace SSS.Service.Test
{
    [TestFixture]
    public class NotificationTests: TestBase
    {
        private Mock<INotificationService> _notificationServiceMock;
        private Mock<INotifier> _notifierMock;
        private NotificationReceiver _testReceiver;
        private IGameStatusNotifier _gameStatusNotifier;

        protected override void Init(Action<ILifetimeScope> setupDependencies = null, bool mockTokenization = true)
        {
            Action<Autofac.ILifetimeScope> localSetup = (container) =>
            {
                _testReceiver = new NotificationReceiver()
                {
                    NotificationId = "testreceivernotificationid",
                    Platform = Shared.NotificationPlatform.Android,
                    UserId = Guid.NewGuid(),
                    UserPublicKey = "abcdefg"
                };

                _notificationServiceMock = new Mock<INotificationService>();
                _notificationServiceMock.Setup(s => s.GetNotificationReceiversForUser("testuser")).Returns(new[] { _testReceiver });
                OverrideDependency(container, _notificationServiceMock.Object);

                _notifierMock = new Mock<INotifier>(MockBehavior.Loose);
                var notificationPlatformHandlerMock = new Mock<INotificationPlatformHandler>();
                notificationPlatformHandlerMock.Setup(n => n.GetNotifierForPlatform(It.IsAny<NotificationPlatform>()))
                                               .Returns(_notifierMock.Object);
                OverrideDependency(container, notificationPlatformHandlerMock.Object);

                _gameStatusNotifier = container.Resolve<IGameStatusNotifier>();

                setupDependencies?.Invoke(container);
            };
            base.Init(localSetup, mockTokenization);
        }
        
        [SetUp]
        public void TestInitialize()
        {
            Init();
        }

        [Test]
        public void TestNewGameNotificationSent()
        {
            var newGame = Game.StartNew("testuser", null, null, "playerb");
            _gameStatusNotifier.NotifyNewGameStarted(newGame, newGame.Challenger);

            _notifierMock.Verify(notifier => notifier.Send(
                _testReceiver.NotificationId,
                It.Is<NewGameStartedNotification>(n => n.GameId.Equals(newGame.GameId) && n.Type == NotificationType.NewGame), 
                It.IsAny<NotificationContent>()));
        }

        [Test]
        public void TestGameUpdatedNotificationSent()
        {
            var key = GetRandomBytes(16);
            var newGame = Game.StartNew("testuser", key, null, "playerb");
            _notificationServiceMock.Setup(s => s.GetNotificationReceiverForGame(key)).Returns(_testReceiver);

            _gameStatusNotifier.NotifyGameStatusChanged(newGame, newGame.Challenger);

            _notifierMock.Verify(notifier => notifier.Send(
                _testReceiver.NotificationId,
                It.Is<GameUpdatedNotification>(n => n.GameId.Equals(newGame.GameId) && n.Type == NotificationType.GameUpdated),
                It.IsAny<NotificationContent>()));
        }
    }
}
