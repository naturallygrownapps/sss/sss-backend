﻿using NUnit.Framework;
using Moq;
using Nancy;
using SSS.Service.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSS.Shared.Parameters;

namespace SSS.Service.Test
{
    [TestFixture]
    public class RegisterTests : TestBase
    {
        [Test]
        public void TestRegisterNewUser()
        {
            UserEntity createdUser = null;
            // Setup
            Init(container =>
                {
                    var userMock = new Mock<IUserService>();
                    userMock.Setup(s => s.AddUser(It.IsAny<UserEntity>())).
                             Callback<UserEntity>(u => createdUser = u);
                    OverrideDependency<IUserService>(container, userMock.Object);
                });

            // Call
            var response = Browser.Post("/auth/register", with =>
                {
                    WithDefaults(with);
                    WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                    {
                        Username = "rufus",
                        PwHash = "abc",
                        Pk = GetRandomBytes(100)
                    });
                });

            // Assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.IsNotNull(createdUser);
            Assert.AreEqual("rufus", createdUser.Username);
        }

        private void TestRegisterInvalidUser(string username)
        {
            var userMock = new Mock<IUserService>();
            Init(container =>
            {
                userMock.Setup(s => s.AddUser(It.IsAny<UserEntity>())).Verifiable();
                OverrideDependency<IUserService>(container, userMock.Object);
            });

            var response = Browser.Post("/auth/register", ctx =>
            {
                WithDefaults(ctx);
                WithJsonBody(ctx, new AuthModuleParameters.RegisterInput()
                {
                    Username = username,
                    PwHash = "abc"
                });
            });

            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);

            userMock.Verify(s => s.AddUser(It.IsAny<UserEntity>()), Times.Never);
        }

        [Test]
        public void TestRegisterInvalidUser_TooShort()
        {
            TestRegisterInvalidUser("tt");
        }

        [Test]
        public void TestRegisterInvalidUser_TooLong()
        {
            TestRegisterInvalidUser(string.Join("", Enumerable.Repeat("t", 26)));
        }

        [Test]
        public void TestRegisterInvalidUser_InvalidChars()
        {
            TestRegisterInvalidUser("!!!");
            TestRegisterInvalidUser("$$$");
            TestRegisterInvalidUser("%%%");
            TestRegisterInvalidUser("&&&");
            TestRegisterInvalidUser("???");
            TestRegisterInvalidUser("[[[");
            TestRegisterInvalidUser("]]]");
            TestRegisterInvalidUser("\"\"\"");
            TestRegisterInvalidUser("'''");
            TestRegisterInvalidUser("\\\\\\");
        }

        [Test]
        public void TestRegisterNonLowercaseUser()
        {
            var username = "RufuS";
            var pw = "abc";
            UserEntity createdUser = null;
            Init(container =>
            {
                var userMock = new Mock<IUserService>();
                userMock.Setup(s => s.AddUser(It.IsAny<UserEntity>())).
                         Callback<UserEntity>(u => createdUser = u);
                userMock.Setup(s => s.FindUser(It.Is<string>(name => name.ToLower() == username.ToLower()))).Returns(() => createdUser);
                OverrideDependency(container, userMock.Object);
            }, mockTokenization: false);

            // Call
            var response = Browser.Post("/auth/register", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                {
                    Username = username,
                    PwHash = pw,
                    Pk = GetRandomBytes(100)
                });
            });

            // Assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            Assert.IsNotNull(createdUser);
            Assert.AreEqual("RufuS", createdUser.Username);
            
            response = Browser.Post("/auth/login", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                {
                    Username = "rufus",
                    PwHash = pw
                });
            });
            var loginResponseBody = DeserializeBody<AuthModuleParameters.LoginOutput>(response);

            response = Browser.Get("/player/self", with =>
            {
                WithDefaults(with);
                with.Header("Authorization", "Token " + loginResponseBody.Token);
            });
            var user = DeserializeBody<Shared.UserEntityDto>(response);
            Assert.AreEqual("RufuS", user.Username);
        }

        [Test]
        public void TestRegisterExistingUser()
        {
            // Setup
            Init(container =>
            {
                var userMock = new Mock<IUserService>();
                userMock.Setup(s => s.FindUser("rufus")).
                         Returns(new UserEntity() { Username = "rufus" });
                OverrideDependency<IUserService>(container, userMock.Object);
            });

            // Call
            var response = Browser.Post("/auth/register", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                {
                    Username = "rufus",
                    PwHash = "abc",
                    Pk = GetRandomBytes(100)
                });
            });

            // Assert
            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void TestRegisterNobody()
        {
            Init(container =>
            {
                var userMock = new Mock<IUserService>();
                userMock.Setup(s => s.FindUser(It.IsAny<string>())).
                         Returns((UserEntity)null);
                OverrideDependency<IUserService>(container, userMock.Object);
            });

            var response = Browser.Post("/auth/register", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                {
                    Username = "nobody",
                    PwHash = "abc",
                    Pk = GetRandomBytes(100)
                });
            });

            Assert.AreEqual(HttpStatusCode.Conflict, response.StatusCode);
        }

        [Test]
        public void TestRegisterUserAndRetrievePrivateKey()
        {
            var username = "rufus";
            var pw = "abc";
            var pk = GetRandomBytes(1000);

            UserEntity createdUser = null;
            Init(container =>
            {
                var userMock = new Mock<IUserService>();
                userMock.Setup(s => s.AddUser(It.IsAny<UserEntity>())).
                         Callback<UserEntity>(u => createdUser = u);
                userMock.Setup(s => s.FindUser(It.Is<string>(name => name.ToLower() == username.ToLower()))).Returns(() => createdUser);
                OverrideDependency(container, userMock.Object);
            });

            Browser.Post("/auth/register", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.RegisterInput()
                {
                    Username = "rufus",
                    PwHash = pw,
                    Pk = pk
                });
            });
            
            var response = Browser.Post("/auth/login", with =>
            {
                WithDefaults(with);
                WithJsonBody(with, new AuthModuleParameters.LoginInput()
                {
                    Username = "rufus",
                    PwHash = pw
                });
            });
            var loginResponseBody = DeserializeBody<AuthModuleParameters.LoginOutput>(response);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            CollectionAssert.AreEqual(pk, loginResponseBody.Pk);
        }
    }
}
