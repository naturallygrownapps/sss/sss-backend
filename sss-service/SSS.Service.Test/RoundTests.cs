﻿using Autofac;
using NUnit.Framework;
using SSS.Service.Gaming;
using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Test
{
    [TestFixture]
    public class RoundTests : TestBase
    {
        private GameParticipant Challenger;
        private GameParticipant Opponent;

        [SetUp]
        public void TestInitialize()
        {
            Init(null);
            Challenger = new GameParticipant()
            {
                Username = "challenger"
            };
            Opponent = new GameParticipant()
            {
                Username = "opponent"
            };
        }

        private Round CreateRound(Move challengerMove,
                                  Move opponentMove)
        {
            return new Round()
            {
                ChallengerMove = challengerMove,
                OpponentMove = opponentMove
            };
        }

        private void SimulateGame(Move challengerMove, Move opponentMove, GameParticipant assertedWinner)
        {
            var round = CreateRound(challengerMove, opponentMove);
            var winner = round.GetWinner(Challenger, Opponent);
            Assert.AreEqual(assertedWinner, winner);
        }

        [Test]
        public void TestPaperRock()
        {
            SimulateGame(Move.Paper, Move.Rock, Challenger);
        }

        [Test]
        public void TestPaperScissors()
        {
            SimulateGame(Move.Paper, Move.Scissors, Opponent);
        }

        [Test]
        public void TestPaperPaper()
        {
            SimulateGame(Move.Paper, Move.Paper, GameParticipant.Nobody);
        }

   
        [Test]
        public void TestRockPaper()
        {
            SimulateGame(Move.Rock, Move.Paper, Opponent);
        }

        [Test]
        public void TestRockScissors()
        {
            SimulateGame(Move.Rock, Move.Scissors, Challenger);
        }

        [Test]
        public void TestRockRock()
        {
            SimulateGame(Move.Rock, Move.Rock, GameParticipant.Nobody);
        }

        [Test]
        public void TestScissorsPaper()
        {
            SimulateGame(Move.Scissors, Move.Paper, Challenger);
        }

        [Test]
        public void TestScissorsRock()
        {
            SimulateGame(Move.Scissors, Move.Rock, Opponent);
        }

        [Test]
        public void TestScissorsScissors()
        {
            SimulateGame(Move.Scissors, Move.Scissors, GameParticipant.Nobody);
        }
    }
}
