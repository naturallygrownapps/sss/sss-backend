﻿using Autofac;
using NUnit.Framework;
using Moq;
using Nancy.Authentication.Token;
using Nancy.Bootstrapper;
using Nancy.Security;
using Nancy.Testing;
using Newtonsoft.Json;
using SSS.Service.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mongo2Go;
using SSS.Service.Storage;
using MongoDB.Driver;
using SSS.Service.Test.Utils;
using SSS.Service.Utils;

namespace SSS.Service.Test
{
    [TestFixture]
    public class TestBase
    {
        private class PublicContainerBootstrapper : Bootstrapper
        {
            private readonly Action<ILifetimeScope> _appContainerConfigCallback;

            public PublicContainerBootstrapper(Action<ILifetimeScope> appContainerConfigCallback)
	        {
                _appContainerConfigCallback = appContainerConfigCallback;
	        }

            public ILifetimeScope GetContainer()
            {
                return ApplicationContainer;
            }

            protected override void ConfigureApplicationContainer(ILifetimeScope container)
            {
 	             base.ConfigureApplicationContainer(container);

                 if (_appContainerConfigCallback != null)
                 {
                     _appContainerConfigCallback(container);
                 }
            }
        }

        private ILifetimeScope _container;
        protected ILifetimeScope Container
        {
            get { return _container; }
        }

        private Bootstrapper _bootstrapper;
        protected Bootstrapper Bootstrapper
        {
            get { return _bootstrapper; }
        }

        private Browser _browser;
        protected Browser Browser
        {
            get { return _browser; }
        }

        private string _currentUsername = null;
        private string _mockUserToken = null;
        private JsonSerializer _jsonSerializer;
        protected JsonSerializer JsonSerializer
        {
            get { return _jsonSerializer; }
        }
        
        protected static Random Random { get; } = new Random();

        private IMockDatabase _mockDatabase = null;

        #region Setup and dependencies

        protected virtual void Init(Action<ILifetimeScope> setupDependencies = null,
                                    bool mockTokenization = true)
        {
            var bs = new PublicContainerBootstrapper((container) =>
            {
                _container = container;
                SetupDatabaseMock();
                if (mockTokenization)
                    PrepareAuthenticationMock(container);
                if (setupDependencies != null)
                    setupDependencies(container);
            });
            _bootstrapper = bs;
            _browser = new Browser(_bootstrapper);
            _container = bs.GetContainer();
            _jsonSerializer = _container.Resolve<JsonSerializer>();
        }

        private void PrepareAuthenticationMock(ILifetimeScope container)
        {
            _mockUserToken = "1234";
            var tokenizerMock = new Mock<ITokenizer>();
            tokenizerMock.Setup(s => s.Tokenize(It.Is<IUserIdentity>(u => u.UserName == _currentUsername),
                                                It.IsAny<Nancy.NancyContext>())).
                          Returns(_mockUserToken);
            tokenizerMock.Setup(s => s.Detokenize(_mockUserToken, It.IsAny<Nancy.NancyContext>(), It.IsAny<Nancy.Authentication.Token.IUserIdentityResolver>())).
                          Returns(() => new UserIdentity() { UserName = _currentUsername });
            OverrideDependency<ITokenizer>(container, tokenizerMock.Object);
        }

        protected void AuthenticateAs(string username)
        {
            _currentUsername = username;
        }
        
        protected void OverrideDependency<TService>(ILifetimeScope container, TService implementation)
            where TService : class
        {
            var builder = new ContainerBuilder();
            builder.RegisterInstance<TService>(implementation);
            builder.Update(container.ComponentRegistry);
        }

        [TearDown]
        protected virtual void Cleanup()
        {
            if (_mockDatabase != null)
            {
                _mockDatabase.Dispose();
                _mockDatabase = null;
            }
        }

        #endregion

        #region HTTP call helpers

        protected void WithDefaults(BrowserContext ctx)
        {
            ctx.HttpRequest();
            ctx.UserHostAddress("127.0.0.1");
            ctx.Accept(new Nancy.Responses.Negotiation.MediaRange("application/json"));
        }

        protected void WithJsonBody(BrowserContext ctx, object body)
        {
            var json = ToJson(body);
            ctx.Body(json, "application/json");
        }

        protected void WithPreconfiguredAuthentication(BrowserContext ctx)
        {
            if (string.IsNullOrWhiteSpace(_mockUserToken))
            {
                throw new Exception("Call PrepareAuthenticationMock during test initialization to use this method.");
            }
            ctx.Header("Authorization", "Token " + _mockUserToken);
        }

        protected string ToJson(object obj)
        {
            using (var writer = new System.IO.StringWriter())
            {
                _jsonSerializer.Serialize(writer, obj);
                return writer.ToString();
            }
        }

        protected T DeserializeBody<T>(BrowserResponse response)
        {
            var reader = new System.IO.StringReader(response.Body.AsString());
            var jsonReader = new Newtonsoft.Json.JsonTextReader(reader);
            var b = JsonSerializer.Deserialize<T>(jsonReader);
            return b;
        }

        protected string Serialize(object o)
        {
            using (var writer = new System.IO.StringWriter())
            {
                JsonSerializer.Serialize(writer, o);
                return writer.ToString();
            }
        }

        protected string GetErrorMessage(BrowserResponse response)
        {
            return DeserializeBody<ErrorResponse.Error>(response).ErrorMessage;
        }

        #endregion

        #region MongoDB
        
        protected IDatabase SetupDatabaseMock()
        {
            if (_mockDatabase == null)
            {
                if (RequiresDatabase)
                    _mockDatabase = new MongoDbMockDatabase();
                else
                    _mockDatabase = new EmptyMockDatabase();
                OverrideDependency<IDatabase>(Container, _mockDatabase);
            }
            return _mockDatabase;
        }

        protected virtual bool RequiresDatabase
        {
            get { return false; }
        }

        #endregion

        #region Other Helpers

        protected byte[] GetRandomBytes(int count)
        {
            var data = new byte[count];
            Random.NextBytes(data);
            return data;
        }

        protected Shared.EncryptedBet GetRandomBet()
        {
            return new Shared.EncryptedBet()
            {
                Data = GetRandomBytes(1000),
                ReceiverKeyAndIV = GetRandomBytes(100),
                SourceKeyAndIV = GetRandomBytes(100)
            };
        }

        #endregion
    }
}
