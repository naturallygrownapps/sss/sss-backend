﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace SSS.Service.Test.Utils
{
    class EmptyMockDatabase : IMockDatabase
    {
        public void Dispose()
        {
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return null;
        }
    }
}
