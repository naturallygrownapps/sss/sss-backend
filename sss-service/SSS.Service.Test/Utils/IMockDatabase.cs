﻿using SSS.Service.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Test.Utils
{
    interface IMockDatabase: IDisposable, IDatabase
    {
    }
}
