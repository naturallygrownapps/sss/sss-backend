﻿using Mongo2Go;
using MongoDB.Driver;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Test.Utils
{
    class MongoDbMockDatabase : IMockDatabase
    {
        private MongoDbRunner _mongoDbRunner;
        private MongoClient _client;
        private IMongoDatabase _db;

        public MongoDbMockDatabase()
        {
            _mongoDbRunner = MongoDbRunner.Start(System.IO.Path.Combine(TestContext.CurrentContext.WorkDirectory, "mongodb"));
            _client = new MongoClient(_mongoDbRunner.ConnectionString);
            _db = _client.GetDatabase("sss");
        }

        public void Dispose()
        {
            if (_mongoDbRunner != null)
            {
                _mongoDbRunner.Dispose();
                _mongoDbRunner = null;
            }
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }
}
