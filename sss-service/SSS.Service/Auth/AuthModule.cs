﻿using MongoDB.Driver;
using Nancy;
using Nancy.Authentication.Token;
using Nancy.ModelBinding;
using Nancy.Security;
using SSS.Service.Notifications;
using SSS.Service.Storage;
using SSS.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AuthParams = SSS.Shared.Parameters.AuthModuleParameters;
using SSS.Shared.Parameters;
using System.Text.RegularExpressions;
using SSS.Service.Utils.Locking;

namespace SSS.Service.Auth
{
    public partial class AuthModule : NancyModule
    {
        private readonly IUserService _userSvc;
        private readonly ITokenizer _tokenizer;
        private readonly INotificationService _notificationSvc;
        private readonly ILockFactory _lockFactory;

        private const string UserRegistrationLock = "UserRegistration";

        /// <summary>
        /// Limits allowed usernames.
        /// </summary>
        private static readonly Regex UsernameRegex = new Regex("^[a-zA-Zäöüß0-9]{3,25}$", RegexOptions.Compiled);

        public AuthModule(IUserService userSvc, ITokenizer tokenizer, INotificationService notificationSvc, ILockFactory lockFactory)
            : base("/auth")
        {
            _userSvc = userSvc;
            _tokenizer = tokenizer;
            _notificationSvc = notificationSvc;
            _lockFactory = lockFactory;
            
            Post["/login"] = _ => this.Call<AuthParams.LoginInput>(Login);
            Post["/register", runAsync: true] = (_, ct) => this.Call<AuthParams.RegisterInput>(Register);
            Post["/registerNotifications"] = _ => this.Call<AuthParams.RegisterNotificationsInput>(RegisterNotifications);
            Post["/deleteNotifications"] = _ => this.Call<AuthParams.DeleteNotificationsInput>(DeleteNotifications);
        }

        private dynamic Login(AuthParams.LoginInput input)
        {
            if (string.IsNullOrWhiteSpace(input.Username) ||
                string.IsNullOrWhiteSpace(input.PwHash))
            {
                return ErrorResponse.FromMessage("Missing username and/or password.", HttpStatusCode.BadRequest);
            }

            var username = input.Username;
            var password = input.PwHash;
            try
            {
                ValidationHelper.ValidateString(username, 50);
                ValidationHelper.ValidateString(password, 1000);
            }
            catch(FormatException)
            {
                return ErrorResponse.FromMessage("Invalid username and/or password.", HttpStatusCode.BadRequest, "AUTH_INVALIDUSERNAME");
            }

            var existingEntity = _userSvc.FindUser(username);

            var scryptEncoder = new Scrypt.ScryptEncoder();
            if (existingEntity != null && scryptEncoder.Compare(password, existingEntity.PwHash))
            {
                var user = new UserIdentity(existingEntity);
                var token = _tokenizer.Tokenize(user, Context);
                return new AuthParams.LoginOutput()
                {
                    Token = token,
                    Username = existingEntity.Username,
                    Pk = existingEntity.Pk
                };
            }
            else
            {
                return HttpStatusCode.Unauthorized;
            }
        }

        private async Task<dynamic> Register(AuthParams.RegisterInput input)
        {
            var username = input.Username;
            var desiredPassword = input.PwHash;
            if (string.IsNullOrWhiteSpace(input.Username) ||
                string.IsNullOrWhiteSpace(input.PwHash))
            {
                return ErrorResponse.FromMessage("Missing username and/or password.", HttpStatusCode.BadRequest);
            }

            if (input.Pk == null || input.Pk.Length == 0)
            {
                return ErrorResponse.FromMessage("Missing pk.", HttpStatusCode.BadRequest);
            }
            
            if (!UsernameRegex.IsMatch(username))
            {
                return ErrorResponse.FromMessage("Invalid username format. Only 3-25 alphanumeric characters are allowed.", HttpStatusCode.BadRequest);
            }
            if (desiredPassword.Length > 1000)
            {
                return ErrorResponse.FromMessage("Requested password is too long.", HttpStatusCode.BadRequest);
            }

            using (var l = await _lockFactory.LockAsyncWithDefaults(UserRegistrationLock + "_" + username))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();

                var existing = _userSvc.FindUser(username);
                if (existing != null ||
                    SSS.Service.Gaming.GameParticipant.Nobody.IsPlayer(username))
                {
                    return ErrorResponse.FromMessage("Sorry, that username is already taken. Please choose a different name.").
                                         WithStatusCode(HttpStatusCode.Conflict);
                }
                else
                {
                    var scryptEncoder = new Scrypt.ScryptEncoder();
                    var hashedPw = scryptEncoder.Encode(desiredPassword);
                    var newUser = new UserEntity()
                    {
                        UserId = Guid.NewGuid(),
                        Username = username,
                        PwHash = hashedPw,
                        Pk = input.Pk
                    };
                    _userSvc.AddUser(newUser);


                    return Negotiate.WithModel(new AuthParams.RegisterOutput()
                    {
                        UserId = newUser.UserId
                    }).WithStatusCode(HttpStatusCode.Created);
                }
            }
        }

        private dynamic RegisterNotifications(AuthParams.RegisterNotificationsInput input)
        {
            this.RequiresAuthentication();
            var curUsername = Context.CurrentUser.UserName;
            var user = _userSvc.FindUser(curUsername);
            if (user != null)
            {
                var userId = user.UserId;
                _notificationSvc.RegisterNotificationReceiver(input.NotificationId, input.UserPublicKey, userId, input.Platform);
                return HttpStatusCode.OK;
            }
            else
            {
                return HttpStatusCode.NotFound;
            }
        }

        private dynamic DeleteNotifications(AuthParams.DeleteNotificationsInput arg)
        {
            this.RequiresAuthentication();
            _notificationSvc.DeleteNotificationReceiverByNotificationToken(arg.NotificationId);
            return HttpStatusCode.OK;
        }
    }
}