﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
namespace SSS.Service.Auth
{
    public interface IUserService
    {
        void AddUser(UserEntity userEntity);
        UserEntity FindUser(string username);
        IEnumerable<UserEntity> FindUsersLike(string username);
    }
}
