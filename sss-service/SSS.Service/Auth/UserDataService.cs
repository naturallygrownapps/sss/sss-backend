﻿using MongoDB.Bson;
using MongoDB.Driver;
using SSS.Service.Storage;
using SSS.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver.Linq;
using System.Text.RegularExpressions;

namespace SSS.Service.Auth
{
    public class UserDataService : SSS.Service.Auth.IUserService
    {
        private readonly IMongoCollection<UserEntity> _userCollection;

        public UserDataService(IDatabase db)
        {
            _userCollection = db.GetCollection<UserEntity>("users");
        }

        public UserEntity FindUser(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return null;
            }

            username = Regex.Escape(username);
            var filter = Builders<UserEntity>.Filter.Regex(u => u.Username, new BsonRegularExpression(new Regex("^" + username + "$", RegexOptions.IgnoreCase)));
            return _userCollection.Find(filter).SingleOrDefault();
        }

        public void AddUser(UserEntity userEntity)
        {
            _userCollection.InsertOne(userEntity);
        }

        public IEnumerable<UserEntity> FindUsersLike(string username)
        {
            username = Regex.Escape(username);
            var filter = Builders<UserEntity>.Filter.Regex(u => u.Username, new BsonRegularExpression(new Regex("^" + username, RegexOptions.IgnoreCase)));
            return _userCollection.Find(filter).Limit(20).ToList();
        }
    }
}