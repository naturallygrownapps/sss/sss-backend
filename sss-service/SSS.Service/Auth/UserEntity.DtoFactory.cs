﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSS.Shared;

namespace SSS.Service.Auth
{
    public partial class UserEntity
    {
        public UserEntityDto CreateDto()
        {
            return new UserEntityDto()
            {
				Username = this.Username
            };
        }
    }
}
