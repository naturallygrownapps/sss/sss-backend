﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Auth
{
    public partial class UserEntity
    {
        public ObjectId Id { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string PwHash { get; set; }
        public byte[] Pk { get; set; }

        public UserEntity()
        {
            Id = MongoDB.Bson.ObjectId.Empty;
        }
    }
}