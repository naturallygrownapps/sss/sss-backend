﻿using Nancy.Bootstrappers.Autofac;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.Bootstrapper;
using SSS.Service.Utils;
using Nancy.Authentication.Token;
using Nancy.Authentication.Token.Storage;
using SSS.Service.Storage;
using SSS.Service.Auth;
using SSS.Service.Gaming;
using log4net;
using Nancy.Diagnostics;
using SSS.Service.Notifications;
using Nancy.Conventions;
using RedLock;
using SSS.Service.Utils.Locking;

namespace SSS.Service
{
    public class Bootstrapper : AutofacNancyBootstrapper
    {
        protected override DiagnosticsConfiguration DiagnosticsConfiguration
        {
            get 
            {
                return new DiagnosticsConfiguration()
                {
                    Password = Environment.GetEnvironmentVariable("SSS_DIAGNOSTICS_PW")
                };
            }
        }

        protected override void ApplicationStartup(ILifetimeScope container, IPipelines pipelines)
        {
            // No registrations should be performed in here, however you may
            // resolve things that are needed during application startup.

            base.ApplicationStartup(container, pipelines);
            
            var logger = LogManager.GetLogger("sss");
            pipelines.OnError.AddItemToEndOfPipeline((context, exception) =>
            {
                logger.Error("Unhandled exception", exception);
                return null;
            });

            // CORS enable.
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                            .WithHeader("Access-Control-Allow-Methods", "POST,GET,DELETE");

                if (ctx.Request.Method == "OPTIONS")
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Headers",
                                            "Accept, Origin, Content-type, Authorization");
                }

            });
        }

        protected override void RequestStartup(ILifetimeScope container, IPipelines pipelines, NancyContext context)
        {
            // No registrations should be performed in here, however you may
            // resolve things that are needed during request startup.

            TokenAuthentication.Enable(pipelines, new TokenAuthenticationConfiguration(container.Resolve<ITokenizer>()));

            pipelines.OnError.AddItemToEndOfPipeline((ctx, ex) =>
            {
                var logger = LogManager.GetLogger("sss");
                logger.Error("Unhandled error on request: " + ctx.Request.Url + " : " + ex.Message, ex);
                ctx.Trace.TraceLog.WriteLog(sb => sb.Append(ex.ToString()));
                var response = ErrorResponse.FromException(ex);
                return response;
            });
        }
        
        protected override void ConfigureApplicationContainer(ILifetimeScope container)
        {
            // Perform registrations that should have an application lifetime.

            var cfg = new Config();
            CheckConfig(cfg);
            RegisterInstances(container, new[] { new InstanceRegistration(typeof(Config), cfg) });

            ConfigureLogging(cfg);
            container.Update(b => b.RegisterModule<LogModule>());
            var log = LogManager.GetLogger("STARTUP");

            log.Info("Running in " + Enum.GetName(typeof(Config.ServerMode), cfg.Mode) + " mode.");

            var typeRegs = new List<TypeRegistration>()
            {
                new TypeRegistration(typeof(IDatabase), typeof(Database)),
                new TypeRegistration(typeof(Newtonsoft.Json.JsonSerializer), typeof(SSSJsonSerializer)),
                new TypeRegistration(typeof(IUserService), typeof(UserDataService)),
                new TypeRegistration(typeof(IGameService), typeof(GameDataService)),
                new TypeRegistration(typeof(INotificationService), typeof(NotificationService)),
                new TypeRegistration(typeof(IGameStatusNotifier), typeof(GameStatusNotifier)),
                new TypeRegistration(typeof(AndroidNotifier), typeof(AndroidNotifier)),
                new TypeRegistration(typeof(INotificationPlatformHandler), typeof(NotificationPlatformHandler))
            };
            
            if (cfg.Mode == Config.ServerMode.Single)
            {
                typeRegs.Add(new TypeRegistration(typeof(ILockFactory), typeof(LocalLockFactory)));
            }
            else
            {
                typeRegs.Add(new TypeRegistration(typeof(ILockFactory), typeof(RedisLockFactoryWrapper)));
            }

            RegisterTypes(container, typeRegs);


            var instanceRegs = new List<InstanceRegistration>
            {
                new InstanceRegistration(typeof(ITokenizer), new NonExpiringTokenizer(LogManager.GetLogger(typeof(NonExpiringTokenizer)), cfg.JwtSecret))
            };
            RegisterInstances(container, instanceRegs);
            
            // Tell the mongodb library to store properties with a starting lower case letter
            var camelCaseConventionPack = new MongoDB.Bson.Serialization.Conventions.ConventionPack { new MongoDB.Bson.Serialization.Conventions.CamelCaseElementNameConvention() };
            MongoDB.Bson.Serialization.Conventions.ConventionRegistry.Register("CamelCase", camelCaseConventionPack, type => true);
        }

        private void CheckConfig(Config cfg)
        {
            var configSanity = cfg.Check();
            var configWarnings = configSanity.GetMessagesOfSeverity(ConfigSanity.Severity.Warning);
            if (!string.IsNullOrEmpty(configWarnings))
            {
                Console.WriteLine("Configuration Warnings: ");
                Console.WriteLine(configWarnings);
            }

            if (configSanity.HasCriticalMessages)
            {
                Console.Error.WriteLine("Critical configuration error found: ");
                Console.Error.WriteLine(configSanity.GetMessagesOfSeverity(ConfigSanity.Severity.Critical));
                Console.Error.WriteLine("Exiting.");
                Environment.Exit(1);
            }
        }

        private void ConfigureLogging(Config cfg)
        {
            // Configure logging.
            bool loggingConfigured = false;
            var loggerFile = new System.IO.FileInfo(System.IO.Path.Combine(DataPathProvider.Instance.GetRootPath(), "log.xml"));
            if (loggerFile.Exists)
            {
                try
                {
                    var logXmlStr = System.IO.File.ReadAllText(loggerFile.FullName);
                    var logFile = string.Empty;
                    if (!string.IsNullOrWhiteSpace(cfg.LogPath))
                    {
                        logFile = System.IO.Path.GetFullPath(cfg.LogPath);
                    }
                    logXmlStr = logXmlStr.Replace("{LOGFILE}", logFile);

                    var xml = new System.Xml.XmlDocument();
                    xml.LoadXml(logXmlStr);
                    log4net.Config.XmlConfigurator.Configure(xml.DocumentElement);
                    if (!string.IsNullOrEmpty(logFile))
                    {
                        Console.WriteLine("Logging to " + logFile);
                    }
                    loggingConfigured = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error configuring logging. " + ex.ToString());
                }
            }

            if (!loggingConfigured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        protected override void ConfigureRequestContainer(ILifetimeScope container, NancyContext context)
        {
            // Perform registrations that should have a request lifetime
        }

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            base.ConfigureConventions(nancyConventions);
            // We don't serve static content here.
            nancyConventions.StaticContentsConventions.Clear();
            nancyConventions.StaticContentsConventions.Add((ctx, path) => null);
        }

    }
}