﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service
{
    public class ConfigSanity
    {
        public enum Severity
        {
            Warning, Critical
        }

        public class Entry
        {
            public string Message;
            public Severity Severity;
        }

        private readonly List<Entry> _entries = new List<Entry>();
        
        public bool HasCriticalMessages
        {
            get { return _entries.Any(e => e.Severity == Severity.Critical); }
        }

        public void CheckIfFilled(string value, string message, Severity severity = Severity.Warning)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                _entries.Add(new Entry() { Message = message, Severity = severity });
            }
        }

        public void CheckWithFunc(Func<bool> validityCheck, string message, Severity severity = Severity.Warning)
        {
            if (!validityCheck())
            {
                _entries.Add(new Entry() { Message = message, Severity = severity });
            }
        }

        public string GetMessagesOfSeverity(Severity severity)
        {
            return string.Join("\n", _entries.Where(e => e.Severity == severity).Select(e => e.Message));
        }
    }

    public class Config
    {
        public enum ServerMode
        {
            Single, Cluster
        }

        public class NotificationsConfig
        {
            private string _gcmAuth;
            public string GcmAuth
            {
                get { return _gcmAuth; }
                set { _gcmAuth = value; }
            }

            private string _gcmSenderId;
            public string GcmSenderId
            {
                get { return _gcmSenderId; }
                set { _gcmSenderId = value; }
            }
        }

        private string _dbConnectionString;
        public string DbConnectionString
        {
            get { return _dbConnectionString; }
            set { _dbConnectionString = value; }
        }

        private string _jwtSecret;
        public string JwtSecret
        {
            get { return _jwtSecret; }
            set { _jwtSecret = value; }
        }

        private string _logPath;
        public string LogPath
        {
            get { return _logPath; }
            set { _logPath = value; }
        }

        private ServerMode _mode = ServerMode.Single;
        public ServerMode Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private string _redisConnection;
        /// <summary>
        /// Format server:port,server:port,option1=value,option2=value... as described here: https://stackexchange.github.io/StackExchange.Redis/Configuration.
        /// </summary>
        public string RedisConnection
        {
            get { return _redisConnection; }
            set { _redisConnection = value; }
        }

        private NotificationsConfig _notifications;
        public NotificationsConfig Notifications
        {
            get { return _notifications; }
            set { _notifications = value; }
        }

        public Config()
        {
            _notifications = new NotificationsConfig();

            try
            {
                var configFilePath = Path.Combine(Utils.DataPathProvider.Instance.GetRootPath(), "config.json");
                var cfgStr = File.ReadAllText(configFilePath);
                JsonConvert.PopulateObject(cfgStr, this);
            }
            catch(Exception ex)
            {
                Console.Error.WriteLine("Could not parse config.json. " + ex.Message);
            }
        }

        public ConfigSanity Check()
        {
            var sanity = new ConfigSanity();
            sanity.CheckIfFilled(this.DbConnectionString, "Missing DbConnectionString", ConfigSanity.Severity.Critical);
            sanity.CheckWithFunc(() => this.Mode == ServerMode.Single || (this.Mode == ServerMode.Cluster && !string.IsNullOrWhiteSpace(this.RedisConnection)),
                                 "No RedisConnection configured in cluster mode.",
                                 ConfigSanity.Severity.Critical);
            sanity.CheckIfFilled(this.Notifications.GcmAuth, "Missing GcmAuth. Cannot send push notifications.");
            sanity.CheckIfFilled(this.Notifications.GcmSenderId, "Missing GcmSenderId. Cannot send push notifications.");
            sanity.CheckIfFilled(this.JwtSecret, "Missing JwtSecret, this server will use a random in-memory key.");
            return sanity;
        }
    }
}
