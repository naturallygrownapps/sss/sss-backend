﻿using Nancy.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using System.IO;
using Newtonsoft.Json;

namespace SSS.Service.Gaming
{
    class CheckedJsonResponse<TModel>: Response
    {
        public CheckedJsonResponse(TModel model, JsonSerializer serializer, Action onSendContentSuccess)
        {
            this.ContentType = "application/json";
            this.StatusCode = HttpStatusCode.OK;
            this.Contents = GetJsonContents(model, serializer, onSendContentSuccess);
        }

        private static Action<Stream> GetJsonContents(TModel model, JsonSerializer serializer, Action onSendContentSuccess)
        {
            return output =>
            {
                using (var w = new StreamWriter(output))
                {
                    serializer.Serialize(w, model);
                }
                onSendContentSuccess?.Invoke();
            };
        }
    }
}
