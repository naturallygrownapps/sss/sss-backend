﻿using MongoDB.Bson;
using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Gaming
{
    public class Game
    {
        public const int RequiredWinningRounds = 3;
        
        public ObjectId Id { get; set; }

        public Guid GameId { get; set; }
        public int Revision { get; set; }

        public bool IsRejected { get; set; }
        public GameParticipant Challenger { get; set; }
        public GameParticipant Opponent { get; set; }
        public BetCapabilities? AgreedBetType { get; set; }
        public BetCapabilities[] BetTypeCaps { get; set; }

        public List<Round> Rounds { get; set; }
        /// <summary>
        /// 0-based
        /// </summary>
        public int CurrentRound { get; set; } = -1;

        public DateTime CreationDate { get; set; }

        public Game()
        {
        }

        public static Game StartNew(string challengerUsername, byte[] challengerPublicKey, BetCapabilities[] challengerBetTypeCaps,
                                    string opponentUsername)
        {
            var game = new Game()
            {
                GameId = Guid.NewGuid(),
                Challenger = new GameParticipant()
                {
                    Username = challengerUsername,
                    PublicKey = challengerPublicKey
                },
                Opponent = new GameParticipant()
                {
                    Username = opponentUsername
                },
                Rounds = new List<Round>(RequiredWinningRounds),
                IsRejected = false,
                BetTypeCaps = challengerBetTypeCaps,
                CreationDate = DateTime.Now
            };

            return game;
        }

        public void Accept(byte[] publicKey, EncryptedBet opponentBet, BetCapabilities betType)
        {
            this.Opponent.PublicKey = publicKey;
            this.Opponent.Bet = opponentBet;
            this.AgreedBetType = betType;
            this.CurrentRound = 0;
            this.Rounds.Add(new Round());
            this.Revision++;
        }

        public void RejectByOpponent()
        {
            IsRejected = true;
            Opponent.HasCompletedGame = true;
            Revision++;
        }

        public void SetChallengerBet(EncryptedBet bet)
        {
            this.Challenger.Bet = bet;
            this.Revision++;
        }

        public GameStatus MakeMove(string currentUsername, Move move)
        {
            if (!IsPlayersTurn(currentUsername))
                throw new GameException("It's not your turn.");
            
            var round = GetCurrentRound();
            if (Challenger.IsPlayer(currentUsername))
            {
                round.ChallengerMove = move;
            }
            else
            {
                round.OpponentMove = move;
            }

            if (round.IsFinished && !IsFinished)
            {
                CurrentRound++;
                var nextRound = new Round();
                Rounds.Add(nextRound);
            }

            this.Revision++;

            return GetStatus(currentUsername);
        }

        public GameStatus GetStatus(string currentUsername)
        {
            IEnumerable<GameParticipant> roundWinners = GetRoundWinners();
            var isPlayerChallenger = Challenger.IsPlayer(currentUsername);

            Func<GameStatus.GameState> DetermineState = () =>
            {
                if (IsRejected)
                {
                    return GameStatus.GameState.Rejected;
                }
                else if (!IsAcceptedByOpponent)
                {
                    return isPlayerChallenger? GameStatus.GameState.InitiatedByYou : GameStatus.GameState.AcceptanceRequired;
                }
                else if (isPlayerChallenger && Challenger.Bet == null)
                {
                    return GameStatus.GameState.ChallengerBetRequired;
                }
                else
                {
                    var winner = GetWinner(roundWinners);
                    if (winner != null)
                    {
                        return winner.Username == currentUsername ? GameStatus.GameState.YouWon : GameStatus.GameState.YouLost;
                    }
                    else
                    {
                        if (IsPlayersTurn(currentUsername))
                        {
                            return GameStatus.GameState.YourMoveRequired;
                        }
                        else
                        {
                            return GameStatus.GameState.WaitingForOtherPlayer;
                        }
                    }
                }
            };

            return new GameStatus()
            {
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                CreationDate = CreationDate,
                GameId = GameId,
                State = DetermineState(),
                RequiredWinningRounds = RequiredWinningRounds,
                RoundWinners = roundWinners.Select(s => s.Username).ToArray(),
                OtherPlayer = (isPlayerChallenger ? Opponent : Challenger).CreateDto(),
                AgreedBetType = AgreedBetType,
                BetTypeCaps = BetTypeCaps,
                Rounds = Rounds.Select(r => r.CreateDto(isPlayerChallenger ? PlayerRole.Challenger : PlayerRole.Opponent)).ToArray(),
                PlayerRole = isPlayerChallenger ? PlayerRole.Challenger : PlayerRole.Opponent,
                Revision = Revision
            };
        }

        public bool IsPlayersTurn(string playerUsername)
        {
            if (IsRejected)
                return false;

            var round = GetCurrentRound();
            if (round == null)
            {
                return false;
            }
            else
            {
                Move? move;
                if (Challenger.IsPlayer(playerUsername))
                    move = round.ChallengerMove;
                else
                    move = round.OpponentMove;

                return !move.HasValue;
            }
        }

        private Round GetCurrentRound()
        {
            return Rounds.Count > 0 ? Rounds[CurrentRound] : null;
        }

        private IEnumerable<GameParticipant> GetRoundWinners()
        {
            if (Rounds == null)
                return Enumerable.Empty<GameParticipant>();

            var roundWinners = new List<GameParticipant>(Rounds.Count);
            foreach (var r in Rounds.Where(r => r.IsFinished))
            {
                var winner = r.GetWinner(Challenger, Opponent);
                roundWinners.Add(winner);
            }
            return roundWinners;
        }

        public EncryptedBet GetPrize()
        {
            var winner = GetWinner();
            if (winner == null)
                return null;
            else
                return winner == Challenger ? Opponent.Bet : Challenger.Bet;
        }

        public GameParticipant GetWinner()
        {
            return GetWinner(null);
        }

        private GameParticipant GetWinner(IEnumerable<GameParticipant> roundWinners)
        {
            if (roundWinners == null)
                roundWinners = GetRoundWinners();

            if (Rounds.Count < RequiredWinningRounds)
                return null;

            int challengerWins = 0;
            int opponentWins = 0;
            foreach (var winner in roundWinners)
            {
                if (winner == Challenger)
                {
                    challengerWins++;
                    if (challengerWins >= RequiredWinningRounds)
                        return Challenger;
                }
                else if (winner == Opponent)
                {
                    opponentWins++;
                    if (opponentWins >= RequiredWinningRounds)
                        return Opponent;
                }
            }

            return null;
        }

        public bool IsLoser(string playerUsername)
        {
            var winner = GetWinner();
            if (winner == null)
                return false;

            GameParticipant loser;
            if (winner == Challenger)
                loser = Opponent;
            else
                loser = Challenger;

            return loser.IsPlayer(playerUsername);
        }

        public bool IsWinner(string playerUsername)
        {
            var winner = GetWinner();
            if (winner == null)
                return false;

            return winner.IsPlayer(playerUsername);
        }

        public bool IsParticipant(string playerUsername)
        {
            return GetParticipant(playerUsername) != null;
        }

        public GameParticipant GetParticipant(string playerUsername)
        {
            if (playerUsername == Challenger.Username)
                return Challenger;
            else if (playerUsername == Opponent.Username)
                return Opponent;
            else
                return null;
        }

        public bool IsFinished
        {
            get { return GetWinner() != null; }
        }

        public bool IsAcceptedByOpponent
        {
            get 
            { 
                // A game is accepted when we have both players' keys.
                return !IsRejected && Opponent.PublicKey != null; 
            }
        }

        public GameParticipant GetOtherPlayer(string currentPlayerName)
        {
            if (Opponent.IsPlayer(currentPlayerName))
                return Challenger;
            else if (Challenger.IsPlayer(currentPlayerName))
                return Opponent;
            else
                return null;
        }

    }
}