﻿using MongoDB.Bson;
using MongoDB.Driver;
using SSS.Service.Storage;
using SSS.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Gaming
{
    public class GameDataService : SSS.Service.Gaming.IGameService
    {
        private readonly IMongoCollection<Game> _gamesCollection;

        public GameDataService(IDatabase database)
        {
            _gamesCollection = database.GetCollection<Game>("games");
        }

        public void AddGame(Game game)
        {
            _gamesCollection.InsertOne(game);
        }

        public void RemoveGame(Game game)
        {
            _gamesCollection.DeleteOne(g => g.Id == game.Id);
        }

        public Game FindGame(Guid gameId)
        {
            return _gamesCollection.Find(g => g.GameId == gameId).SingleOrDefault();
        }

        public void UpdateGame(Game game)
        {
            _gamesCollection.ReplaceOne(g => g.Id == game.Id, game);
        }
        
        public IEnumerable<Game> FindGamesForUser(string username, byte[] gameKey)
        {
            var builder = Builders<Game>.Filter;
            var opponentFilter = builder.Eq(g => g.Challenger.Username, username) & builder.Eq(g => g.Challenger.PublicKey, gameKey);
            var challengerFilter = builder.Eq(g => g.Opponent.Username, username) & builder.Where(g => g.Opponent.PublicKey == null || gameKey.Equals(g.Opponent.PublicKey));

            return _gamesCollection.Find(opponentFilter | challengerFilter).ToList();
        }
    }
}