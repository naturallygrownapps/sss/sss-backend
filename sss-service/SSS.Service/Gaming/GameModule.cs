﻿using MongoDB.Bson;
using Nancy;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using SSS.Service.Utils;
using SSS.Service.Auth;
using GameParams = SSS.Shared.Parameters.GameModuleParameters;
using SSS.Shared;
using SSS.Service.Notifications;
using SSS.Service.Utils.Locking;
using System.Threading.Tasks;

namespace SSS.Service.Gaming
{
    public partial class GameModule : NancyModule
    {
        private readonly IGameService _gameSvc;
        private readonly IUserService _userSvc;
        private readonly IGameStatusNotifier _gameStatusChangedNotifier;
        private readonly Newtonsoft.Json.JsonSerializer _serializer;
        private readonly ILockFactory _lockFactory;

        public GameModule(
            IGameService gameSvc, 
            IUserService userSvc, 
            IGameStatusNotifier gameStatusChangedNotifier, 
            Newtonsoft.Json.JsonSerializer serializer,
            ILockFactory lockFactory)
            : base("/game")
        {
            _gameSvc = gameSvc;
            _userSvc = userSvc;
            _gameStatusChangedNotifier = gameStatusChangedNotifier;
            _serializer = serializer;
            _lockFactory = lockFactory;

            this.RequiresAuthentication();

            Get["/{GameId}"] = _ => this.Call<GameParams.QueryInput>(Query);
            Get["/{GameId}/bet"] = _ => this.Call<GameParams.GetBetInput>(GetBet);

            Post["/challenge"] = _ => this.Call<GameParams.ChallengeInput>(Challenge);
            Post["/{GameId}/challengerbet", runAsync: true] = (_, ct) => this.Call<GameParams.ChallengerBetInput>(ChallengerBet);
            Post["/{GameId}/opponentaccept", runAsync: true] = (_, ct) => this.Call<GameParams.OpponentAcceptInput>(OpponentAccept);
            Post["/{GameId}/opponentreject", runAsync: true] = (_, ct) => this.Call<GameParams.OpponentRejectInput>(OpponentReject);
            Post["/{GameId}/acceptrejection", runAsync: true] = (_, ct) => this.Call<GameParams.AcceptRejectionInput>(AcceptRejection);
            Post["/{GameId}/move", runAsync: true] = (_, ct) => this.Call<GameParams.MoveInput>(Move);

            Post["/mygames"] = _ => this.Call<GameParams.MyGamesInput>(GetMyGames);

        }

        private Task<ILock> LockForGame(Guid gameId)
        {
            return _lockFactory.LockAsyncWithDefaults("Game_" + GuidTools.ToString(gameId));
        }

        private dynamic Challenge(GameParams.ChallengeInput input)
        {
            // We don't lock this function. The worst thing that can happen is that players have two concurrent games
            // which is acceptable. Locking is difficult here as we would have to lock on the (player1, player2) tuple.

            var existingOpponent = _userSvc.FindUser(input.ChallengedUsername);
            if (existingOpponent == null)
            {
                return ErrorResponse.FromMessage("The challenged player does not exist.")
                                    .WithStatusCode(HttpStatusCode.NotFound);
            }

            var curUsername = Context.CurrentUser.UserName;

            if (existingOpponent.Username == curUsername)
            {
                return ErrorResponse.FromMessage("You cannot challenge yourself.")
                                    .WithStatusCode(HttpStatusCode.BadRequest);
            }

            if (input.BetTypeCaps == null || !input.BetTypeCaps.Any())
            {
                return ErrorResponse.FromMessage("No bet type capabilities specified. Cannot play without providing a prize in case of loss.")
                                    .WithStatusCode(HttpStatusCode.BadRequest);
            }
            
            var activeGamesWithOpponent = _gameSvc.FindGamesForUser(curUsername, input.PublicKey)
                                                  .Where(g => !g.IsFinished && !g.IsRejected && g.IsParticipant(input.ChallengedUsername));
            var existingGame = activeGamesWithOpponent.FirstOrDefault();
            if (existingGame != null)
            {
                return Negotiate
                    .WithHeader("Location", "/game/" + GuidTools.ToString(existingGame.GameId))
                    .WithStatusCode(HttpStatusCode.SeeOther)
                    .WithModel(existingGame.GetStatus(curUsername));
            }

            var game = Game.StartNew(curUsername, input.PublicKey, input.BetTypeCaps, existingOpponent.Username);
            _gameSvc.AddGame(game);

            _gameStatusChangedNotifier.NotifyNewGameStarted(game, game.GetOtherPlayer(curUsername));

            return Negotiate.WithModel(game.GetStatus(Context.CurrentUser.UserName))
                                .WithStatusCode(HttpStatusCode.Created);
        }

        private async Task<dynamic> OpponentAccept(GameParams.OpponentAcceptInput input)
        {
            using (var l = await LockForGame(input.GameId))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();

                var game = _gameSvc.FindGame(input.GameId);
                if (game == null)
                    return NoSuchGameError();
                if (game.IsFinished)
                    return GameFinishedError();
                if (game.IsAcceptedByOpponent)
                    return GameAlreadyAcceptedError();

                var curUsername = Context.CurrentUser.UserName;
                if (curUsername != game.Opponent.Username)
                    return AllowedOnlyByOpponentError();

                if (!game.BetTypeCaps.Contains(input.BetType))
                    return ErrorResponse.FromMessage("Your opponent does not support a bet of type " +
                                                     Enum.GetName(typeof(BetCapabilities), input.BetType) + ".")
                                        .WithStatusCode(HttpStatusCode.BadRequest);

                game.Accept(input.PublicKey, input.Bet, input.BetType);
                _gameSvc.UpdateGame(game);

                _gameStatusChangedNotifier.NotifyGameStatusChanged(game, game.GetOtherPlayer(curUsername));

                return Negotiate.WithModel(game.GetStatus(curUsername));
            }
        }

        private async Task<dynamic> ChallengerBet(GameParams.ChallengerBetInput input)
        {
            using (var l = await LockForGame(input.GameId))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();

                var game = _gameSvc.FindGame(input.GameId);
                if (game == null)
                    return NoSuchGameError();
                if (game.IsFinished)
                    return GameFinishedError();
                if (!game.IsAcceptedByOpponent)
                    return GameNotAcceptedError();

                var curUsername = Context.CurrentUser.UserName;
                if (!game.Challenger.IsPlayer(curUsername))
                {
                    return ErrorResponse.FromMessage("This method can only be called by the challenger of a game.",
                                                     HttpStatusCode.Forbidden);
                }

                game.SetChallengerBet(input.Bet);
                _gameSvc.UpdateGame(game);

                _gameStatusChangedNotifier.NotifyGameStatusChanged(game, game.GetOtherPlayer(curUsername));

                return HttpStatusCode.OK;
            }
        }

        private async Task<dynamic> OpponentReject(GameParams.OpponentRejectInput input)
        {
            using (var l = await LockForGame(input.GameId))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();

                var game = _gameSvc.FindGame(input.GameId);
                if (game == null)
                    return NoSuchGameError();
                if (game.IsFinished)
                    return GameFinishedError();
                // Games can only be rejected when not accepted yet.
                // Otherwise players would simply cancel games to circumvent a loss.
                if (game.IsAcceptedByOpponent)
                    return GameAlreadyAcceptedError();

                var curUsername = Context.CurrentUser.UserName;
                if (curUsername != game.Opponent.Username)
                    return AllowedOnlyByOpponentError();

                game.RejectByOpponent();
                _gameSvc.UpdateGame(game);

                _gameStatusChangedNotifier.NotifyGameRejected(game, game.GetOtherPlayer(curUsername));

                return Negotiate.WithModel(game.GetStatus(curUsername));
            }
        }

        /// <summary>
        /// When your opponent rejected a game, you can only accept the rejection to remove the game.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private async Task<dynamic> AcceptRejection(GameParams.AcceptRejectionInput input)
        {
            using (var l = await LockForGame(input.GameId))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();
                
                var game = _gameSvc.FindGame(input.GameId);
                if (game == null)
                    return NoSuchGameError();
                if (!game.IsRejected)
                    return ErrorResponse.FromMessage("This can only be called for rejected games.").
                                         WithStatusCode(HttpStatusCode.BadRequest);

                var curUsername = Context.CurrentUser.UserName;
                if (curUsername != game.Challenger.Username)
                    return AllowedOnlyByChallengerError();
                
                _gameSvc.RemoveGame(game);

                return Negotiate.WithStatusCode(HttpStatusCode.NoContent);
            }
        }

        private async Task<dynamic> Move(GameParams.MoveInput input)
        {
            using (var l = await LockForGame(input.GameId))
            {
                if (!l.HasAcquiredOrFailed())
                    return ErrorResponse.LockingError();

                var game = _gameSvc.FindGame(input.GameId);
                if (game == null)
                    return NoSuchGameError();
                if (!game.IsAcceptedByOpponent)
                    return GameNotAcceptedError();
                var curUsername = Context.CurrentUser.UserName;
                if (curUsername != game.Opponent.Username &&
                    curUsername != game.Challenger.Username)
                {
                    return AllowedOnlyByParticipantError();
                }

                if (game.Challenger.IsPlayer(curUsername) && game.Challenger.Bet == null)
                {
                    return ErrorResponse.FromMessage("You have to place your bet before making a move.", HttpStatusCode.BadRequest);
                }

                if (!game.IsPlayersTurn(curUsername))
                    return ErrorResponse.FromMessage("It's not your turn.")
                                        .WithStatusCode(HttpStatusCode.Forbidden);

                game.MakeMove(curUsername, input.Move);
                _gameSvc.UpdateGame(game);

                _gameStatusChangedNotifier.NotifyGameStatusChanged(game, game.GetOtherPlayer(curUsername));

                return Negotiate.WithModel(game.GetStatus(curUsername));
            }
        }

        private dynamic Query(GameParams.QueryInput input)
        {
            var game = _gameSvc.FindGame(input.GameId);
            if (game == null)
                return NoSuchGameError();
            var curUsername = Context.CurrentUser.UserName;
            if (curUsername != game.Opponent.Username &&
                curUsername != game.Challenger.Username)
            {
                return AllowedOnlyByParticipantError();
            }

            var status = game.GetStatus(curUsername);

            return Negotiate.WithModel(status);
        }

        private dynamic GetBet(GameParams.GetBetInput input)
        {
            var game = _gameSvc.FindGame(input.GameId);
            if (game == null)
                return NoSuchGameError();
            if (!game.IsFinished)
                return GameNotFinishedError();

            var curUsername = Context.CurrentUser.UserName;
            if (!game.IsParticipant(curUsername))
            {
                return ErrorResponse.FromMessage("This method can only be called by a participant of the game.").
                                        WithStatusCode(HttpStatusCode.Forbidden);
            }

            var participant = game.GetParticipant(curUsername);
            if (participant.HasCompletedGame)
            {
                return ErrorResponse.FromMessage("The bet for a game can only be claimed once.").
                                        WithStatusCode(HttpStatusCode.Forbidden);
            }

            var prize = game.GetPrize();

            // The game will only be set as "completed" when the response content was successfully sent.
            // This prevents unwanted deleted games e.g. when the network connectivity drops.
            return new CheckedJsonResponse<EncryptedBet>(prize, _serializer, async () =>
            {
                using (var l = await LockForGame(input.GameId))
                {
                    // Should check if the lock was acquired, but what do we do if it failed?
                    // The game has to be updated as the user already got his reponse...

                    // Get the game again, it might have changed.
                    game = _gameSvc.FindGame(input.GameId);
                    participant = game.GetParticipant(curUsername);

                    // Response was successfully sent.
                    participant.HasCompletedGame = true;

                    // Both players have obtained their bet, remove the game.
                    if (game.Challenger.HasCompletedGame && game.Opponent.HasCompletedGame)
                    {
                        _gameSvc.RemoveGame(game);
                    }
                    else
                    {
                        _gameSvc.UpdateGame(game);
                    }
                }
            });
        }
        
        private dynamic GetMyGames(GameParams.MyGamesInput input)
        {
            var curUsername = Context.CurrentUser.UserName;
            var games = _gameSvc.FindGamesForUser(curUsername, input.GameKey);
            return games
                // Do not show games that the user cannot interact with anymore.
                .Where(g => !g.GetParticipant(curUsername).HasCompletedGame)
                .Select(g => g.GetStatus(curUsername));
        }

        private Response NoSuchGameError()
        {
            return ErrorResponse.FromMessage("This game does not exist.").
                                 WithStatusCode(HttpStatusCode.NotFound);
        }

        private Response GameNotFinishedError()
        {
            return ErrorResponse.FromMessage("This action is not allowed when the game is not finished yet.").
                                 WithStatusCode(HttpStatusCode.Forbidden);
        }

        private Response GameFinishedError()
        {
            return ErrorResponse.FromMessage("This action is not allowed when the game has finished.").
                                 WithStatusCode(HttpStatusCode.Forbidden);
        }

        private Response GameAlreadyAcceptedError()
        {
            return ErrorResponse.FromMessage("This game has already been accepted.")
                                .WithStatusCode(HttpStatusCode.Forbidden);
        }

        private Response GameNotAcceptedError()
        {
            return ErrorResponse.FromMessage("This game has not been accepted by the opponent yet.")
                                .WithStatusCode(HttpStatusCode.Forbidden);
        }

        private Response AllowedOnlyByOpponentError()
        {
            return ErrorResponse.FromMessage("This action can only be performed by the challenged player.").
                                 WithStatusCode(HttpStatusCode.Forbidden);
        }
        
        private Response AllowedOnlyByChallengerError()
        {
            return ErrorResponse.FromMessage("This action can only be performed by the challenging player.").
                                 WithStatusCode(HttpStatusCode.Forbidden);
        }

        private Response AllowedOnlyByParticipantError()
        {
            return ErrorResponse.FromMessage("This action can only be performed by a participating player.").
                                 WithStatusCode(HttpStatusCode.Forbidden);
        }
    }
}