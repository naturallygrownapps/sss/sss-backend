﻿using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Gaming
{
    public partial class GameParticipant
    {
        public GameParticipantDto CreateDto()
        {
            return new GameParticipantDto()
            {
                Username = this.Username,
                PublicKey = this.PublicKey
            };
        }
    }
}
