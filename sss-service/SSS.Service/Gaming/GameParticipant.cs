﻿using MongoDB.Bson;
using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Gaming
{
    public partial class GameParticipant
    {
        /// <summary>
        /// Represents a non-existing player.
        /// </summary>
        public static readonly GameParticipant Nobody = new GameParticipant()
        {
            Username = "nobody"
        };

        public string Username { get; set; }
        public byte[] PublicKey { get; set; }
        public EncryptedBet Bet { get; set; }
        /// <summary>
        /// Gets or sets whether a game is finished and this participant has looked at his price/loss.
        /// This is also set when you rejected a game.
        /// If this is true, the game will not be visible for the player anymore.
        /// </summary>
        public bool HasCompletedGame { get; set; }

        public GameParticipant()
        {
        }

        public bool IsPlayer(string username)
        {
            return this.Username != null && this.Username.ToLower() == username.ToLower();
        }
    }
}