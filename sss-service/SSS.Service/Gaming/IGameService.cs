﻿using System;
using System.Collections.Generic;
namespace SSS.Service.Gaming
{
    public interface IGameService
    {
        void AddGame(Game game);
        Game FindGame(Guid gameId);
        void RemoveGame(Game game);
        void UpdateGame(Game game);
        IEnumerable<Game> FindGamesForUser(string username, byte[] gameKey);
    }
}
