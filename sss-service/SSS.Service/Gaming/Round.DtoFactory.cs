﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSS.Shared;

namespace SSS.Service.Gaming
{
    public partial class Round
    {
        public RoundDto CreateDto(PlayerRole playerRole)
        {
            var roundDto = new RoundDto();
            roundDto.YourMove = playerRole == PlayerRole.Challenger ? this.ChallengerMove : this.OpponentMove;
            roundDto.OtherPlayerMove = playerRole == PlayerRole.Challenger ? this.OpponentMove : this.ChallengerMove;
            return roundDto;
        }
    }
}
