﻿using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SSS.Service.Gaming
{
    /// <summary>
    /// Represents one round of a game, i.e. a single turn in which each player provides a <see cref="Move"/> (rock, paper or scissors).
    /// </summary>
    public partial class Round
    {
        private Move? _challengerMove;
        public Move? ChallengerMove
        {
            get { return _challengerMove; }
            set { _challengerMove = value; }
        }

        private Move? _opponentMove;
        public Move? OpponentMove
        {
            get { return _opponentMove; }
            set { _opponentMove = value; }
        }

        public Round()
        {
        }

        /// <summary>
        /// Determines the winner of a round.
        /// </summary>
        /// <returns>The winning <see cref="GameParticipant"/> or <see cref="GameParticipant.Nobody"/> if the round is a draw.</returns>
        /// <exception cref="GameException">Thrown when the round is not finished yet.</exception>
        /// <exception cref="InvalidOperationException">Thrown when one of the players has made an unknown <see cref="Move"/>.</exception>
        public GameParticipant GetWinner(GameParticipant challenger, GameParticipant opponent)
        {
            if (!IsFinished)
                throw new GameException("Cannot determine a winner or loser when the round is not finished yet.");

            Move p1Move = ChallengerMove.Value;
            Move p2Move = OpponentMove.Value;
            if (p1Move == p2Move)
            {
                return GameParticipant.Nobody;
            }
            else if (p1Move == Move.Paper)
            {
                if (p2Move == Move.Rock)
                    return challenger;
                else if (p2Move == Move.Scissors)
                    return opponent;
            }
            else if (p1Move == Move.Rock)
            {
                if (p2Move == Move.Paper)
                    return opponent;
                else if (p2Move == Move.Scissors)
                    return challenger;
            }
            else if (p1Move == Move.Scissors)
            {
                if (p2Move == Move.Paper)
                    return challenger;
                else if (p2Move == Move.Rock)
                    return opponent;
            }

            throw new InvalidOperationException("Unknown move.");
        }

        public bool IsFinished
        {
            get { return ChallengerMove.HasValue && OpponentMove.HasValue; }
        }
    }
}