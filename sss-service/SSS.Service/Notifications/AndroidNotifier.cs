﻿using log4net;
using Newtonsoft.Json.Linq;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Notifications
{
    public class AndroidNotifier : IDisposable, INotifier
    {
        private GcmServiceBroker _broker;
        private bool _brokerStarted = false;
        private readonly ILog _log;
        private readonly INotificationService _notificationService;
        private readonly Newtonsoft.Json.JsonSerializer _serializer;

        public AndroidNotifier(ILog log, INotificationService notificationService, Config cfg, Newtonsoft.Json.JsonSerializer serializer)
        {
            _log = log;
            _notificationService = notificationService;
            _serializer = serializer;

            // Configuration
            var config = new GcmConfiguration(cfg.Notifications.GcmSenderId, cfg.Notifications.GcmAuth, null)
            {
                GcmUrl = "https://fcm.googleapis.com/fcm/send"
            };

            // Create a new broker
            _broker = new GcmServiceBroker(config);

            _broker.OnNotificationFailed += OnNotificationFailed;
            _broker.OnNotificationSucceeded += OnNotificationSucceeded;
        }

        public void Send(string notificationUserId, object data, NotificationContent notificationContent = null)
        {
            lock (_broker)
            {
                if (!_brokerStarted)
                {
                    _broker.Start();
                    _brokerStarted = true;
                }
            }

            var gcmNotification = new GcmNotification()
            {
                To = notificationUserId,
                Data = JObject.FromObject(data, _serializer),
                Priority = GcmNotificationPriority.High
            };

            if (notificationContent != null)
            {
                gcmNotification.Notification = JObject.FromObject(notificationContent, _serializer);
            }

            _broker.QueueNotification(gcmNotification);
        }

        private void OnNotificationSucceeded(GcmNotification notification)
        {
        }

        private void OnNotificationFailed(GcmNotification notification, AggregateException exception)
        {
            exception.Handle(ex =>
            {
                // See what kind of exception it was to further diagnose
                if (ex is GcmNotificationException)                {
                    var notificationException = ex as GcmNotificationException;
                    // Deal with the failed notification
                    var gcmNotification = notificationException.Notification;
                    var description = notificationException.Description;

                    _log.Error($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                }
                else if (ex is GcmMulticastResultException)
                {
                    var multicastException = ex as GcmMulticastResultException;
                    foreach (var succeededNotification in multicastException.Succeeded)
                    {
                        _log.Debug($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");
                    }

                    foreach (var failedKvp in multicastException.Failed)
                    {
                        var n = failedKvp.Key;
                        var e = failedKvp.Value;
                        var description = (e as GcmNotificationException)?.Description ?? e.Message;

                        _log.Error($"GCM Notification Failed: ID={n.MessageId}, Desc={description}");
                    }

                }
                else if (ex is DeviceSubscriptionExpiredException)
                {
                    var expiredException = ex as DeviceSubscriptionExpiredException;
                    var oldId = expiredException.OldSubscriptionId;
                    var newId = expiredException.NewSubscriptionId;

                    _log.Debug($"Device RegistrationId Expired: {oldId}");

                    _notificationService.DeleteNotificationReceiverByNotificationToken(oldId);

                    if (!string.IsNullOrWhiteSpace(newId))
                    {
                        // If this value isn't null, our subscription changed and we should update our database
                        _log.Debug($"Device RegistrationId Changed To: {newId}");
                        UpdateNotificationId(oldId, newId);
                    }
                }
                else if (ex is RetryAfterException)
                {
                    var retryException = ex as RetryAfterException;
                    // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                    _log.Error($"GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                }
                else
                {
                    _log.Error("GCM Notification Failed for some unknown reason: " + ex.Message);
                }

                // Mark it as handled
                return true;
            });

        }

        private void UpdateNotificationId(string oldId, string newId)
        {
            _notificationService.UpdateNotificationReceiver(oldId, newId);
        }

        public void Dispose()
        {
            if (_brokerStarted)
            {
                _broker?.Stop();
                _brokerStarted = false;
            }
        }
    }
}
