﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SSS.Shared;
using log4net;

namespace SSS.Service.Notifications
{
    public class GameStatusNotifier : IGameStatusNotifier
    {
        private readonly INotificationService _notificationService;
        private readonly INotificationPlatformHandler _notificationPlatformHandler;
        private readonly ILog _log;

        public GameStatusNotifier(INotificationService notificationService, INotificationPlatformHandler notificationPlatformHandler, ILog log)
        {
            _notificationService = notificationService;
            _notificationPlatformHandler = notificationPlatformHandler;
            _log = log;
        }

        public void NotifyNewGameStarted(Gaming.Game game, Gaming.GameParticipant participant)
        {
            if (participant == null)
            {
                _log.Error("Cannot notify null user.");
                return;
            }

            var allReceiversForUser = _notificationService.GetNotificationReceiversForUser(participant.Username);
            if (!allReceiversForUser.Any())
            {
                _log.Warn("No NotificationReceiver found for user " + participant.Username);
            }
            else
            {
                var opponent = game.GetOtherPlayer(participant.Username);
                foreach (var r in allReceiversForUser)
                {
                    TrySendNotification(r, 
                        new NewGameStartedNotification()
                        {
                            GameId = game.GameId
                        },
                        new NotificationContent()
                        {
                            TitleLocKey = "notification_gamestarted_title",
                            TitleLocArgs = new[] { opponent.Username },
                            BodyLocKey = "notification_gamestarted_body",
                            BodyLocArgs = new[] { opponent.Username },
                            Tag = "sss_started"
                        }
                    );
                }
            }
        }

        public void NotifyGameStatusChanged(Gaming.Game game, Gaming.GameParticipant participant)
        {
            if (participant == null)
            {
                _log.Error("Cannot notify null user.");
                return;
            }

            var notificationReceiver = _notificationService.GetNotificationReceiverForGame(participant.PublicKey);
            var opponent = game.GetOtherPlayer(participant.Username);
            if (!TrySendNotification(notificationReceiver, 
                new GameUpdatedNotification()
                {
                    GameId = game.GameId
                },
                new NotificationContent()
                {
                    TitleLocKey = "notification_gameupdated_title",
                    TitleLocArgs = new[] { opponent.Username },
                    BodyLocKey = "notification_gameupdated_body",
                    BodyLocArgs = new[] { opponent.Username },
                    Tag = "sss_updated"
                })
            )
            {
                _log.Warn("No NotificationRecveiver found for user " + participant.Username);
            }
        }

        public void NotifyGameRejected(Gaming.Game game, Gaming.GameParticipant participant)
        {
            if (participant == null)
            {
                _log.Error("Cannot notify null user.");
                return;
            }

            var notificationReceiver = _notificationService.GetNotificationReceiverForGame(participant.PublicKey);
            var opponent = game.GetOtherPlayer(participant.Username);
            if (!TrySendNotification(notificationReceiver, 
                new GameRejectedNotification()
                {
                    GameId = game.GameId
                },
                new NotificationContent()
                {
                    TitleLocKey = "notification_gamerejected_title",
                    TitleLocArgs = new[] { opponent.Username },
                    BodyLocKey = "notification_gamerejected_body",
                    BodyLocArgs = new[] { opponent.Username },
                    Tag = "sss_rejected"
                }))
            {
                _log.Warn("No NotificationRecveiver found for user " + participant.Username);
            }
        }

        private bool TrySendNotification(NotificationReceiver notificationReceiver, object data, NotificationContent notificationContent = null)
        {
            if (notificationReceiver != null)
            {
                var notifier = _notificationPlatformHandler.GetNotifierForPlatform(notificationReceiver.Platform);
                if (notifier != null)
                {
                    notifier.Send(notificationReceiver.NotificationId, data, notificationContent);
                    return true;
                }
                else
                {
                    _log.Warn("Could not find a notifier implementation for platform " + notificationReceiver.Platform.ToString());
                    return false;
                }
            }

            return false;
        }
    }
}
