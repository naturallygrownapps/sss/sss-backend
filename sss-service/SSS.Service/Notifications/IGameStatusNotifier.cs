﻿using SSS.Service.Gaming;

namespace SSS.Service.Notifications
{
    public interface IGameStatusNotifier
    {
        void NotifyGameStatusChanged(Game game, GameParticipant participant);
        void NotifyNewGameStarted(Game game, GameParticipant participant);
        void NotifyGameRejected(Game game, GameParticipant participant);
    }
}