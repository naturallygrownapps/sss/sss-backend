﻿using SSS.Service.Gaming;
using SSS.Shared;
using System.Collections.Generic;

namespace SSS.Service.Notifications
{
    public interface INotificationService
    {
        void DeleteNotificationReceiverByNotificationToken(string notificationUserId);
        void DeleteNotificationReceiverByPublicKey(string userPublicKey);
        IEnumerable<NotificationReceiver> GetNotificationReceiversForUser(string username);
        NotificationReceiver GetNotificationReceiverForGame(byte[] userPublicKey);
        void RegisterNotificationReceiver(string notificationUserId, string userPublicKey, System.Guid userId, NotificationPlatform platform);
        void UpdateNotificationReceiver(string oldNotificationId, string newNotificationId);
    }
}