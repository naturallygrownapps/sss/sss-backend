﻿using Newtonsoft.Json.Linq;

namespace SSS.Service.Notifications
{
    public interface INotifier
    {
        void Send(string notificationUserId, object data, NotificationContent notification = null);
    }
}