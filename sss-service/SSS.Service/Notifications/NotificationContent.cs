﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Notifications
{
    /// <summary>
    /// Content for the notification payload of a GCM push message (the content displayed e.g. in the android status bar).
    /// See https://firebase.google.com/docs/reference/admin/node/admin.messaging.NotificationMessagePayload
    /// </summary>
    public class NotificationContent
    {
        public string Body { get; set; }
        public string Title { get; set; }

        [JsonProperty(PropertyName = "body_loc_key")]
        public string BodyLocKey { get; set; }

        [JsonProperty(PropertyName = "body_loc_args")]
        public string[] BodyLocArgs { get; set; }

        [JsonProperty(PropertyName = "title_loc_key")]
        public string TitleLocKey { get; set; }

        [JsonProperty(PropertyName = "title_loc_args")]
        public string[] TitleLocArgs { get; set; }

        public string Tag { get; set; }
    }
}
