﻿using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Notifications
{
    public interface INotificationPlatformHandler
    {
        INotifier GetNotifierForPlatform(NotificationPlatform platform);
    }

    public class NotificationPlatformHandler: INotificationPlatformHandler
    {
        private readonly AndroidNotifier _androidNotifier;

        public NotificationPlatformHandler(AndroidNotifier androidNotifier)
        {
            _androidNotifier = androidNotifier;
        }

        public INotifier GetNotifierForPlatform(NotificationPlatform platform)
        {
            if (platform == NotificationPlatform.Android)
                return _androidNotifier;
            else
                return null;
        }
    }
}
