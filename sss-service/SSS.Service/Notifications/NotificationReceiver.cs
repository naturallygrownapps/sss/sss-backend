﻿using MongoDB.Bson;
using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Notifications
{
    public class NotificationReceiver
    {
        public ObjectId Id { get; set; }
        public string NotificationId { get; set; }
        public string UserPublicKey { get; set; }
        public Guid UserId { get; set; }
        public NotificationPlatform Platform { get; set; }
    }
}
