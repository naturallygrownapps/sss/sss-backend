﻿using MongoDB.Driver;
using SSS.Service.Storage;
using SSS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Notifications
{
    class NotificationService : INotificationService
    {
        private readonly IMongoCollection<NotificationReceiver> _notificationReceivers;
        private readonly Auth.IUserService _userService;

        public NotificationService(IDatabase db, Auth.IUserService userService)
        {
            _notificationReceivers = db.GetCollection<NotificationReceiver>("notificationReceivers");
            _userService = userService;
        }

        public void DeleteNotificationReceiverByNotificationToken(string notificationUserId)
        {
            _notificationReceivers.DeleteMany(r => r.NotificationId == notificationUserId);
        }

        public void DeleteNotificationReceiverByPublicKey(string userPublicKey)
        {
            _notificationReceivers.DeleteMany(r => r.UserPublicKey == userPublicKey);
        }

        public void RegisterNotificationReceiver(string notificationUserId, string userPublicKey, Guid userId, NotificationPlatform platform)
        {
            // There must only be one game key registered per app installation.
            DeleteNotificationReceiverByNotificationToken(notificationUserId);
            DeleteNotificationReceiverByPublicKey(userPublicKey);
            _notificationReceivers.InsertOne(new NotificationReceiver()
            {
                NotificationId = notificationUserId,
                UserPublicKey = userPublicKey,
                UserId = userId,
                Platform = platform
            });
        }

        public IEnumerable<NotificationReceiver> GetNotificationReceiversForUser(string username)
        {
            var user = _userService.FindUser(username);
            if (user != null)
            {
                var userId = user.UserId;
                return _notificationReceivers.Find(r => r.UserId == userId).ToList();
            }
            else
            {
                return Enumerable.Empty<NotificationReceiver>();
            }
        }

        public NotificationReceiver GetNotificationReceiverForGame(byte[] userPublicKey)
        {
            var publicKey = Convert.ToBase64String(userPublicKey);
            return _notificationReceivers.Find(r => r.UserPublicKey == publicKey).FirstOrDefault();
        }

        public void UpdateNotificationReceiver(string oldNotificationId, string newNotificationId)
        {
            _notificationReceivers.UpdateOne(r => r.NotificationId == oldNotificationId, Builders<NotificationReceiver>.Update.Set(r => r.NotificationId, newNotificationId));
        }
    }
}
