﻿using Mono.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service
{
    class Options
    {
        public string DataPath { get; private set; }
        private bool _showHelp = false;

        public Options()
        {
        }

        public bool Parse(string[] args)
        {
            var options = new OptionSet
            {
                { "d|dataPath=", "The path to the data directory. The application needs write permissions to this directory.", d => DataPath = d },
                { "h|help", "Show this message and exit.", h => _showHelp = h != null },
            };

            try
            {
                options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            if (_showHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                return false;
            }

            return true;
        }
    }
}
