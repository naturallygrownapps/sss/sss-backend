﻿using Nancy;
using Nancy.Security;
using SSS.Service.Auth;
using SSS.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Modules
{
    public class PlayersModule : NancyModule
    {
        private readonly IUserService _userSvc;

        public PlayersModule(IUserService userSvc)
            : base("/player")
        {
            _userSvc = userSvc;
            this.RequiresAuthentication();

            Get["/self"] = GetPlayerSelf;
            Get["/find/{username}"] = _ => FindPlayers(_.username);
        }

        private dynamic GetPlayerSelf(dynamic parameters)
        {
            var user = _userSvc.FindUser(Context.CurrentUser.UserName);
            if (user != null)
                return user.CreateDto();
            else
                return ErrorResponse.FromMessage("Could not find you. How can this happen?", HttpStatusCode.InternalServerError);
        }

        private dynamic FindPlayers(string username)
        {
            return _userSvc.FindUsersLike(username.ToLower()).Select(u => u.CreateDto());
        }
    }
}