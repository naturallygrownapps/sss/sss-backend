﻿namespace SSS.Service
{
    using System;
    using Owin;
    using Microsoft.Owin.Hosting;

    class Program
    {
        public static Options Options = new Options();

        static void Main(string[] args)
        {
            if (!Options.Parse(args))
            {
                return;
            }

            if (!string.IsNullOrEmpty(Options.DataPath))
            {
                Console.WriteLine("Using data directory " + Options.DataPath);
            }

            var url = "http://+:4420";
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Running on {0}", url);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
            }
        }
    }

    public class Startup
    {
        public void Configuration(Owin.IAppBuilder app)
        {
            app.UseNancy(new Nancy.Owin.NancyOptions()
            {
                Bootstrapper = new Bootstrapper()
            });
        }
    }
}
