﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Storage
{
    public class Database : IDatabase
    {
        private IMongoClient _client;
        private IMongoDatabase _db;

        public Database(Config cfg)
        {
            var connStr = cfg.DbConnectionString;
            _client = new MongoClient(connStr);
            _db = _client.GetDatabase("sss");
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }
    }
}