﻿using MongoDB.Driver;

namespace SSS.Service.Storage
{
    public interface IDatabase
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }
}