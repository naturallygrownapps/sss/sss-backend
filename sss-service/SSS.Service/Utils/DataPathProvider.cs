﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils
{
    class DataPathProvider : IRootPathProvider
    {
        private static Lazy<DataPathProvider> _instance = new Lazy<DataPathProvider>(true);
        public static DataPathProvider Instance => _instance.Value;

        public string GetRootPath()
        {
            var path = Program.Options.DataPath;
            return string.IsNullOrEmpty(path) ? AppDomain.CurrentDomain.BaseDirectory : System.IO.Path.GetFullPath(path);
        }
    }
}
