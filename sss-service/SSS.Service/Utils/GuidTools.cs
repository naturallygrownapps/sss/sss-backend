﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Utils
{
    public class GuidTools
    {
        public static string GetGuid()
        {
            return ToString(Guid.NewGuid());
        }

        public static string ToString(Guid guid)
        {
            return guid.ToString("N");
        }
    }
}