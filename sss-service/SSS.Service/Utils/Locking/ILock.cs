﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    public interface ILock: IDisposable
    {
        string LockId { get; }
        bool IsAcquired { get; }
        bool HasError { get; }
    }
}
