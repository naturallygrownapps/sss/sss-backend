﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    public static class ILockExtensions
    {
        /// <summary>
        /// Returns whether a lock was successfully acquired or if acquiring it failed.
        /// We just continue on failures as if the lock was successful so that the whole system isn't down on 
        /// distributed locking errors. Some broken games can be accepted.
        /// </summary>
        public static bool HasAcquiredOrFailed(this ILock @lock)
        {
            return @lock.IsAcquired || @lock.HasError;
        }
    }
}
