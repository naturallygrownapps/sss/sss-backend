﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    public interface ILockFactory: IDisposable
    {
        Task<ILock> LockAsync(string resource, TimeSpan expiryTime, TimeSpan waitTime, TimeSpan retryTime);
    }
}
