﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    public class LocalLockFactory : ILockFactory
    {
        private class LocalLock : ILock
        {
            public string LockId { get; private set; }
            public bool IsAcquired { get; private set; }
            private LocalLockFactory _creator;
            private DateTime? _expiry;

            public bool IsExpired
            {
                get { return _expiry.HasValue && DateTime.Now > _expiry.Value; }
            }

            public bool HasError
            {
                get { return false; }
            }

            public LocalLock(string lockId, DateTime expiry, LocalLockFactory creator)
            {
                IsAcquired = true;
                LockId = lockId;
                _creator = creator;
                _expiry = expiry;
            }

            public LocalLock()
            {
                IsAcquired = false;
            }

            public void Dispose()
            {
                if (IsAcquired)
                    _creator.Unlock(LockId);
            }
        }

        private readonly ILog _log;
        private readonly object _lock = new object();
        private readonly HashSet<string> _lockedResources = new HashSet<string>();
        private readonly Dictionary<string, LocalLock> _activeLocks = new Dictionary<string, LocalLock>();

        private readonly TimeSpan ExpiredLockCheckInterval;
        private readonly CancellationTokenSource ExpiredLockCollectionCancellationSource = new CancellationTokenSource();

        public LocalLockFactory(ILog log)
            : this(log, TimeSpan.FromMinutes(5))
        {
        }

        public LocalLockFactory(ILog log, TimeSpan expiredLockCheckInterval)
        {
            _log = log;
            ExpiredLockCheckInterval = expiredLockCheckInterval;
            // Start a background tasks that periodically checks for expired locks.
            Task.Factory.StartNew(ExpiredLockCollection, ExpiredLockCollectionCancellationSource.Token, ExpiredLockCollectionCancellationSource.Token);
        }
        
        private async void ExpiredLockCollection(object state)
        {
            var ct = (CancellationToken)state;
            while(!ct.IsCancellationRequested)
            {
                await Task.Delay(ExpiredLockCheckInterval);
                try
                {
                    lock (_lock)
                    {
                        if (_activeLocks.Count > 0)
                        {
                            List<string> toRemove = null;

                            foreach (var kv in _activeLocks)
                            {
                                if (kv.Value.IsExpired)
                                {
                                    if (toRemove == null)
                                        toRemove = new List<string>();
                                    toRemove.Add(kv.Key);
                                }
                            }

                            if (toRemove != null && toRemove.Count > 0)
                            {
                                foreach (var id in toRemove)
                                    Unlock(id);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    _log.Error("Could not clean up expired locks.", ex);
                }
            }
        }

        public async Task<ILock> LockAsync(string resource, TimeSpan expiryTime, TimeSpan waitTime, TimeSpan retryTime)
        {
            DateTime start = DateTime.Now;

            bool lockAcquired = false;
            while(!lockAcquired && (DateTime.Now - start) < waitTime)
            {
                var createdLock = TryLock(resource, expiryTime, out lockAcquired);
                if (lockAcquired)
                    return createdLock;
                else
                    await Task.Delay(retryTime);
            }

            // These are just empty locks signaling that we did not obtain a lock, we don't need to track them.
            return new LocalLock();
        }

        private LocalLock TryLock(string resource, TimeSpan expiryTime, out bool lockAcquired)
        {
            lockAcquired = false;

            lock (_lock)
            {
                if (!IsLocked(resource))
                {
                    _lockedResources.Add(resource);
                    lockAcquired = true;
                }

                if (lockAcquired)
                {
                    var createdLock = new LocalLock(resource, DateTime.Now + expiryTime, this);
                    LocalLock existingLock;
                    if (_activeLocks.TryGetValue(resource, out existingLock))
                    {
                        // This should not happen.
                        existingLock.Dispose();
                    }
                    _activeLocks[resource] = createdLock;
                    return createdLock;
                }
            }

            return null;

        }

        private void Unlock(string resource)
        {
            lock(_lock)
            {
                _lockedResources.Remove(resource);
                _activeLocks.Remove(resource);
            }
        }

        public bool IsLocked(string resource)
        {
            lock(_lock)
            {
                return _lockedResources.Contains(resource);
            }
        }

        public void Dispose()
        {
            // Stop the background task that checks for expired locks.
            ExpiredLockCollectionCancellationSource.Cancel();
        }
    }
}
