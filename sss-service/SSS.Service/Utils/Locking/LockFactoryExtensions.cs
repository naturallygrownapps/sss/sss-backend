﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    public static class LockFactoryExtensions
    {
        public static readonly TimeSpan Expiry = TimeSpan.FromMinutes(1);
        public static readonly TimeSpan RetryInterval = TimeSpan.FromSeconds(1);
        public static readonly TimeSpan MaxWait = TimeSpan.FromSeconds(30);

        public static Task<ILock> LockAsyncWithDefaults(this ILockFactory lockFactory, string resource)
        {
            return lockFactory.LockAsync(resource, Expiry, MaxWait, RetryInterval);
        }
    }
}
