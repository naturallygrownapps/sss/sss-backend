﻿using RedLock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils.Locking
{
    class RedisLockFactoryWrapper : ILockFactory
    {
        class RedisLockWrapper : ILock
        {
            public string LockId => _wrapped.LockId;
            public bool IsAcquired => _wrapped.IsAcquired;
            public bool HasError => _wrapped.HasError();

            private readonly IRedisLock _wrapped;

            public RedisLockWrapper(IRedisLock wrapped)
            {
                _wrapped = wrapped;
            }

            public void Dispose()
            {
                _wrapped.Dispose();
            }
        }

        private RedisLockFactory _wrapped;

        private static RedisLockEndPoint[] ParseRedisEndpoint(string redisConnectionCfg)
        {
            var parsed = StackExchange.Redis.ConfigurationOptions.Parse(redisConnectionCfg);
            return parsed.EndPoints.Select(ep => 
            {
                var lockEp = new RedisLockEndPoint();
                lockEp.EndPoint = ep;
                if (!string.IsNullOrWhiteSpace(parsed.Password))
                    lockEp.Password = parsed.Password;
                return lockEp;
            }).ToArray();
        }

        public RedisLockFactoryWrapper(Config config)
        {
            _wrapped = new RedisLockFactory(ParseRedisEndpoint(config.RedisConnection));
        }

        public void Dispose()
        {
            _wrapped.Dispose();
        }

        public async Task<ILock> LockAsync(string resource, TimeSpan expiryTime, TimeSpan waitTime, TimeSpan retryTime)
        {
            var redisLock = await _wrapped.CreateAsync(resource, expiryTime, waitTime, retryTime);
            return new RedisLockWrapper(redisLock);
        }
    }
}
