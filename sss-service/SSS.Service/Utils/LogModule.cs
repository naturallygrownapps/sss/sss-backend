﻿using Autofac;
using Autofac.Core;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils
{
    public class LogModule: Module
    {
        public LogModule()
        {
        }

        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            var type = registration.Activator.LimitType;
            if (HasPropertyDependencyOnLogger(type))
            {
                registration.Activated += InjectLoggerViaProperty;
            }

            if (HasConstructorDependencyOnLogger(type))
            {
                registration.Preparing += InjectLoggerViaConstructor;
            }
        }

        private ILog CreateLogFor(Type type)
        {
            return LogManager.GetLogger(type);
        }

        private bool HasPropertyDependencyOnLogger(Type type)
        {
            return type.GetProperties().Any(property => property.CanWrite && property.PropertyType == typeof(ILog));
        }

        private bool HasConstructorDependencyOnLogger(Type type)
        {
            return type.GetConstructors()
                       .SelectMany(constructor => constructor.GetParameters()
                                                             .Where(parameter => parameter.ParameterType == typeof(ILog)))
                       .Any();
        }

        private void InjectLoggerViaProperty(object sender, ActivatedEventArgs<object> @event)
        {
            var type = @event.Instance.GetType();
            var propertyInfo = type.GetProperties().First(x => x.CanWrite && x.PropertyType == typeof(ILog));
            propertyInfo.SetValue(@event.Instance, CreateLogFor(type), null);
        }

        private void InjectLoggerViaConstructor(object sender, PreparingEventArgs @event)
        {
            var type = @event.Component.Activator.LimitType;
            @event.Parameters = @event.Parameters.Union(new[]
            {
                new ResolvedParameter((parameter, context) => parameter.ParameterType == typeof(ILog), (p, i) => CreateLogFor(type))
            });
        }
    }
}
