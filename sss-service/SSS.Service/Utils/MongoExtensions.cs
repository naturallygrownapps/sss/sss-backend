﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Utils
{
    public static class MongoExtensions
    {
        public static WriteConcernResult CheckSuccess(this WriteConcernResult result)
        {
            if (result.HasLastErrorMessage)
            {
                throw new MongoException(result.LastErrorMessage);
            }
            return result;
        }

        public static ReplaceOneResult CheckSuccess(this ReplaceOneResult result)
        {
            if (result.IsAcknowledged)
            {
                throw new MongoException("ReplaceOne did not succeed.");
            }
            return result;
        }
    }
}