﻿using Nancy;
using Nancy.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Utils
{
    public static class NancyModuleExtensions
    {
        public static dynamic Call<TInput>(this INancyModule module, Func<TInput, dynamic> handler)
        {
            //using (System.IO.StreamReader sr = new System.IO.StreamReader(module.Request.Body))
            //{
            //    var str = sr.ReadToEnd();
            //    var o = Newtonsoft.Json.JsonConvert.DeserializeObject<TInput>(str);
            //}
           
            var input = module.Bind<TInput>();
            return handler(input);
        }
    }
}