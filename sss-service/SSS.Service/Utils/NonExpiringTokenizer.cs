﻿using log4net;
using Nancy;
using Nancy.Authentication.Token;
using Nancy.ErrorHandling;
using Nancy.Security;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils
{
    /// <summary>
    /// Implementation of <see cref="ITokenizer"/> that never expires the keys.
    /// </summary>
    public class NonExpiringTokenizer : ITokenizer
    {
        private Encoding encoding = Encoding.UTF8;
        private string claimsDelimiter = "|";
        private string hashDelimiter = ":";
        private string itemDelimiter = Environment.NewLine;
        private Func<DateTime> tokenStamp = () => DateTime.UtcNow;

        private byte[] key;

        private Func<NancyContext, string>[] additionalItems =
        {
            ctx => ctx.Request.UserHostAddress
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="NonExpiringTokenizer"/> class.
        /// </summary>
        public NonExpiringTokenizer(ILog log, string keyBase64)
        {
            this.key = InitializeKey(log, keyBase64);
        }

        private byte[] InitializeKey(ILog log, string keyBase64)
        {
            byte[] secretKey = null;
            if (string.IsNullOrEmpty(keyBase64))
            {
                log.Error("Token key is empty. Generating a random key.");
            }
            else
            {
                try
                {
                    secretKey = Convert.FromBase64String(keyBase64);
                    if (secretKey.Length != 64)
                    {
                        secretKey = null;
                        throw new Exception("Invalid key length.");
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Could not parse token key: " + ex.Message + " Generating a random key.");
                }
            }

            if (secretKey == null)
            {
                secretKey = new byte[64];
                using (var rng = new RNGCryptoServiceProvider())
                {
                    rng.GetBytes(secretKey);
                }
            }
            return secretKey;
        }

        /// <summary>
        /// Creates a token from a <see cref="IUserIdentity"/>.
        /// </summary>
        /// <param name="userIdentity">The user identity from which to create a token.</param>
        /// <param name="context">Current <see cref="NancyContext"/>.</param>
        /// <returns>The generated token.</returns>
        public string Tokenize(IUserIdentity userIdentity, NancyContext context)
        {
            var items = new List<string>
            {
                userIdentity.UserName,
                string.Join(this.claimsDelimiter, userIdentity.Claims),
                this.tokenStamp().Ticks.ToString(CultureInfo.InvariantCulture)
            };

            if (this.additionalItems != null)
            {
                foreach (var item in this.additionalItems.Select(additionalItem => additionalItem(context)))
                {
                    if (string.IsNullOrWhiteSpace(item))
                    {
                        throw new RouteExecutionEarlyExitException(new Response { StatusCode = HttpStatusCode.Unauthorized });
                    }
                    items.Add(item);
                }
            }

            var message = string.Join(this.itemDelimiter, items);
            var token = CreateToken(message);
            return token;
        }

        /// <summary>
        /// Creates a <see cref="IUserIdentity"/> from a token.
        /// </summary>
        /// <param name="token">The token from which to create a user identity.</param>
        /// <param name="context">Current <see cref="NancyContext"/>.</param>
        /// <param name="userIdentityResolver">The user identity resolver.</param>
        /// <returns>The detokenized user identity.</returns>
        public IUserIdentity Detokenize(string token, NancyContext context, IUserIdentityResolver userIdentityResolver)
        {
            var tokenComponents = token.Split(new[] { this.hashDelimiter }, StringSplitOptions.None);
            if (tokenComponents.Length != 2)
            {
                return null;
            }

            var messagebytes = Convert.FromBase64String(tokenComponents[0]);
            var hash = Convert.FromBase64String(tokenComponents[1]);

            if (!this.IsValid(messagebytes, hash))
            {
                return null;
            }

            var items = this.encoding.GetString(messagebytes).Split(new[] { this.itemDelimiter }, StringSplitOptions.None);

            if (this.additionalItems != null)
            {
                var additionalItemCount = additionalItems.Count();
                for (var i = 0; i < additionalItemCount; i++)
                {
                    var tokenizedValue = items[i + 3];
                    var currentValue = additionalItems.ElementAt(i)(context);
                    if (tokenizedValue != currentValue)
                    {
                        // todo: may need to log here as this probably indicates hacking
                        return null;
                    }
                }
            }

            //var generatedOn = new DateTime(long.Parse(items[2]));

            //if (tokenStamp() - generatedOn > tokenExpiration())
            //{
            //    return null;
            //}

            var userName = items[0];
            var claims = items[1].Split(new[] { this.claimsDelimiter }, StringSplitOptions.None);

            return userIdentityResolver.GetUser(userName, claims, context);
        }

        private string CreateToken(string message)
        {
            var messagebytes = this.encoding.GetBytes(message);
            var hash = this.GenerateHash(messagebytes);
            return Convert.ToBase64String(messagebytes) + this.hashDelimiter + Convert.ToBase64String(hash);
        }

        private byte[] GenerateHash(byte[] message)
        {
            using (var hmac = new HMACSHA256(key))
            {
                return hmac.ComputeHash(message);
            }
        }

        private bool IsValid(byte[] message, byte[] hash)
        {
            var expectedHash = GenerateHash(message);
            return hash.SequenceEqual(expectedHash);
        }
    }
}
