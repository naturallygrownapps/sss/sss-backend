﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSS.Service.Utils
{
    public class SSSJsonSerializer : JsonSerializer
    {
        public SSSJsonSerializer()
        {
            this.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            this.NullValueHandling = NullValueHandling.Ignore;
            this.Formatting = Formatting.None;
        }
    }
}