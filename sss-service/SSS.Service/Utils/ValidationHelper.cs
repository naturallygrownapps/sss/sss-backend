﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSS.Service.Utils
{
    public static class ValidationHelper
    {
        public static string ValidateString(string str, int length)
        {
            if (string.IsNullOrEmpty(str))
                throw new FormatException("Value must not be empty.");

            if (str.Length <= length)
                return str;
            
            throw new FormatException("Value must not be longer than " + length);
        }
    }
}
